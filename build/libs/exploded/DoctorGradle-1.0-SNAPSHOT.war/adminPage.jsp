<%@ page import="models.User" %>
<%@ page import="models.Doctor" %>
<%@ page import="DAO.DoctorDAO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>AdminPage</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/main.css">
</head>
<body style="background: #FFFFFF">

<div class="container">
    <h2>Doctors</h2>
    <br>
    <div class="boxHidden text-center">
        <span class="errorMessage" style="color: red; font-size: 20px;">${errors}</span>
        <span style="color: #3c763d; font-size: 20px;">${success}</span>
    </div>
    <hr>
    <table class="table table-striped">
        <thead>
        <tr class="doctorTable">
            <th>ID</th>
            <th >First Name</th>
            <th>Last Name</th>
            <th >Start Time</th>
            <th >End Time</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="doctor" items="${doctors}">
            <tr class="row100 body">
                <td class="cell100 column1"><c:out value="${doctor.getId()}" /></td>
                <td class="cell100 column2"><c:out value="${doctor.firstName}"/></td>
                <td class="cell100 column3"><c:out value="${doctor.lastName}" /></td>
                <td class="cell100 column4"><c:out value="${doctor.startTime}" /></td>
                <td class="cell100 column5"><c:out value="${doctor.endTime}" /></td>
                <td>                <form method="post" action="${pageContext.request.contextPath}/adminDelete">
                    <input type="hidden" value="${doctor.id}" name="idDoctor">
                    <input type="submit" class="action-button" style="width: 50%" value="Delete">
                </form></td>
            </tr>
        </c:forEach>
        </tbody>

    </table>
    <br>
    <hr>
    <br>

    <h2>Patients</h2>
    <br>
    <table class="table table-striped">
        <thead>
        <tr class="patientTable">
            <th>ID</th>
            <th >First Name</th>
            <th>Last Name</th>
            <th >D.O.B</th>
            <th >Phone</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="patient" items="${patients}">
            <tr class="row100 body">
                <td class="cell100 column1"><c:out value="${patient.getId()}" /></td>
                <td class="cell100 column2"><c:out value="${patient.firstName}"/></td>
                <td class="cell100 column3"><c:out value="${patient.lastName}" /></td>
                <td class="cell100 column4"><c:out value="${patient.birthDate}" /></td>
                <td class="cell100 column5"><c:out value="${patient.phone}" /></td>
                <td>                <form method="post" action="${pageContext.request.contextPath}/adminDeletePatient">
                    <input type="hidden" value="${patient.getId()}" name="idPatient">
                    <input type="submit" class="action-button" style="width: 50%" value="Delete">
                </form></td>
            </tr>
        </c:forEach>

        </tbody>

    </table>
    <br>
    <hr>
    <br>

    <h2>TimeSlots</h2>
    <br>
    <table class="table table-striped">
        <thead>
        <tr class="timeSlotTable">
            <th>ID</th>
            <th>Doctor</th>
            <th>Patient</th>
            <th>StartTime</th>
            <th>EndTime</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        <c:forEach var="timeSlot" items="${timeSlots}">
            <tr class="row100 body">
                <td class="cell100 column1"><c:out value="${timeSlot.getId()}" /></td>
                <td class="cell100 column2"><c:out value="${timeSlot.getDoctor().getId()}"/></td>
                <td class="cell100 column2"><c:out value="${timeSlot.getPatient().getId()}"/></td>
                <td class="cell100 column3"><c:out value="${timeSlot.from}" /></td>
                <td class="cell100 column4"><c:out value="${timeSlot.to}" /></td>
                <td> <form method="post" action="${pageContext.request.contextPath}/adminDeleteTimeSlot">
                    <input type="hidden" value="${timeSlot.getId()}" name="idTimeSlot">
                    <input type="submit" class="action-button" style="width: 50%" value="Delete">
                </form></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

</div>
<div class="text-center"><button class="btn btn-danger"><a href="addDoctor.jsp">Add Doctor</a></button></div>
<br>
<br>
<script>
    setTimeout(function () {
        $('.boxHidden').fadeOut('fast')
    }, 6000);
</script>
</body>
</html>
