<%@ page import="models.User" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dente &mdash; Colorlib Website Template</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">

</head>
<body style="background: url(images/hero_bg_1.jpg) no-repeat fixed; width: 100%;height: 100%;background-size: 100%;">

<div class="site-wrap">

    <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->


    <div class="site-navbar-wrap">
        <div class="site-navbar-top">
            <div class="container py-2">
                <div class="row align-items-center">
                    <div class="col-6">

                    </div>
                </div>
            </div>
        </div>
        <div class="site-navbar">
            <div class="container py-1">
                <div class="row align-items-center">
                    <div class="col-2">
                        <h2 class="mb-0 site-logo"><a href="login.jsp">DoctorServise</a></h2>
                    </div>
                    <div class="col-10">
                        <nav class="site-navigation text-right" role="navigation">
                            <div class="container">
                                <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>
                                <ul class="site-menu js-clone-nav d-none d-lg-block">
                                    <li><a href="aboutAs.jsp">About Us</a></li>
                                    <li><a href="<c:url value="/allDoctors"/>">Doctors</a></li>
                                    <li class="has-children">
                                        <a href="patients.html">Log in</a>
                                        <ul class="dropdown arrow-top">
                                            <% if (session.getAttribute("User") == null ) {%>
                                            <li><a href ="<c:url value = "/login.jsp"/>" >Log in</a></li>
                                            <li><a href="<c:url value = "/reg.jsp"/>" >Registration</a></li>
                                            <%}%>
                                            <% User user = (User) session.getAttribute("User");
                                                if (user != null && user.getRole()!=null){%>
                                            <li class=""><a href="<c:url value="/logout"/>" class="nav-link">Log out</a>
                                                <b>${user.getRole().toString()} ${user.getFirstName()} ${user.getLastName()}</b></li>
                                            <%}%>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <%--<footer class="site-footer border-top">--%>
        <%--<div class="container">--%>
            <%--<div class="row">--%>
                <%--<div class="col-lg-4 mb-5 mb-lg-0">--%>
                <%--</div>--%>
                <%--<div class="col-lg-4 mb-5 mb-lg-0">--%>
                    <%--<div class="row">--%>
                        <%--<div class="col-md-12">--%>
                            <%--<div>--%>
                                <%--<a href="#" class="pl-0 pr-lg-5"><span class="icon-facebook"></span></a>--%>
                                <%--<a href="#" class="pl-3 pr-lg-5"><span class="icon-twitter"></span></a>--%>
                                <%--<a href="#" class="pl-3 pr-lg-5"><span class="icon-instagram"></span></a>--%>
                                <%--<a href="#" class="pl-3 pr-lg-5"><span class="icon-linkedin"></span></a>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                <%--</div>--%>
            <%--</div>--%>
        <%--</div>--%>
    <%--</footer>--%>
</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>
<script src="js/aos.js"></script>

<script src="js/main.js"></script>

</body>
</html>