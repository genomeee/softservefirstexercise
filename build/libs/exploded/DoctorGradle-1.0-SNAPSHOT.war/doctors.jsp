<%@ page import="models.User" %>
<%@ page import="models.Doctor" %>
<%@ page import="models.Patient" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>AllDoctors</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/flaticon.css"/>
    <link rel="stylesheet" href="css/owl.carousel.css"/>
    <link rel="stylesheet" href="css/magnific-popup.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

</head>
<body>
<div class="site-wrap">

    <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    <div class="site-navbar-wrap">
        <div class="site-navbar-top">
            <div class="container py-2">
                <div class="row align-items-center">
                    <div class="col-6">

                    </div>
                </div>
            </div>
        </div>
        <div class="site-navbar">
            <div class="container py-1">
                <div class="row align-items-center">
                    <div class="col-2">
                        <h2 class="mb-0 site-logo"><a href="login.jsp">DoctorServise</a></h2>
                    </div>
                    <div class="col-10">
                        <nav class="site-navigation text-right" role="navigation">
                            <div class="container">
                                <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#"
                                                                                              class="site-menu-toggle js-menu-toggle text-white"><span
                                        class="icon-menu h3"></span></a></div>
                                <ul class="site-menu js-clone-nav d-none d-lg-block">
                                    <li><a href="aboutAs.jsp">Home</a></li>
                                    <li><a href="<c:url value = "/allDoctors"/>">Doctors</a></li>
                                    <li class="has-children">
                                        <a href="patients.html">Log in</a>
                                        <ul class="dropdown arrow-top">
                                            <% if (session.getAttribute("User") == null) {%>
                                            <li><a href="<c:url value = "/login.jsp"/>">Log in</a></li>
                                            <li><a href="<c:url value = "/reg.jsp"/>">Registration</a></li>
                                            <%}%>
                                            <% User user = (User) session.getAttribute("User");
                                                if (user != null && !(user.getRole().isRoleAdmin())) {%>
                                            <li class=""><a href="<c:url value="/logout"/>" class="nav-link">Log
                                                out </a></li>
                                            <li class=""><a
                                                    href="<c:url value="/patient"/>"><%=user.getFirstName() %>   <%=user.getLastName() %>
                                            </a></li>
                                            <%}%>
                                            <% if (user != null && (user.getRole().isRoleAdmin())) {%>
                                            <li class=""><a href="<c:url value="/logout"/>" class="nav-link">Log
                                                out </a></li>
                                            <li class=""><a
                                                    href="<c:url value="/admin"/>">ADMIN
                                            </a></li>
                                            <%}%>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-blocks-cover inner-page" style="background-image: url(images/hero_bg_1.jpg);" data-aos="fade"
         data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <h1>Our <strong>Doctors</strong></h1>
                </div>
            </div>
        </div>
    </div>
    <%--<% if (request.getAttribute("errors") != null) {%>--%>
    <%--<div class="error-doctor">--%>
        <%--${errors[0]}--%>
    <%--</div>--%>
    <%--<% } %>--%>
    <div class="boxHidden text-center">
        <span class="errorMessage" style="color: red; font-size: 40px;">${errors[0]}</span>
        <span style="color: #3c763d; font-size: 40px;">${success[0]}</span>
    </div>
    <div class="site-section">
        <div class="container">
            <div class="row mb-5">

                <c:forEach items="${doctors}" var="doctor">
                    <div class="col-md-6 col-lg-4 mb-4">
                        <div class="post-entry bg-white">
                            <div class="text p-4">
                                <h2 class="h5 text-black"><a href="#">
                                    <div class="text-center"><c:out value="${doctor.firstName}"/> <c:out
                                            value="${doctor.lastName}"/></div>
                                </a></h2>
                                <span class="text-uppercase date d-block mb-3"><div class="text-center"> <small>Work time :  <c:out
                                        value="${doctor.startTime}"/> - <c:out value="${doctor.endTime}"/></small></div></span>an>
                                <ul class="list-group list-group-flush">
                                    <div class="text-center">
                                        <h4>Doctor's Appointments</h4>
                                        <br>
                                    <c:forEach items="${doctor.timeList}" var="doctorList">
                                        <li class="list-group-item list-group-item-action"><c:out
                                                value="${doctorList.from}"/> --<c:out value="${doctorList.to}"/></li>
                                    </c:forEach>
                                    </div>
                                </ul>
                                <%if (user!= null && (user.getRole().isRolePatient())) {%>
                                <<br>
                                <div class="text-center">
                                    <button type="button" class="btn btn-dark btn-sm mClass" data-id="${doctor.id}"
                                            data-toggle="modal" data-target="#exampleModal">GET
                                        APPOINTMENT
                                    </button>
                                </div>
                                <% } %>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>

    <footer class="site-footer border-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mb-5 mb-lg-0">
                </div>
                <div class="col-lg-4 mb-5 mb-lg-0">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <a href="#" class="pl-0 pr-lg-5"><span class="icon-facebook"></span></a>
                                <a href="#" class="pl-3 pr-lg-5"><span class="icon-twitter"></span></a>
                                <a href="#" class="pl-3 pr-lg-5"><span class="icon-instagram"></span></a>
                                <a href="#" class="pl-3 pr-lg-5"><span class="icon-linkedin"></span></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </footer>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabe1"><strong>GET APPOINTMENT</strong></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="${pageContext.request.contextPath}/addTimeSlot">
                <div class="modal-body">
                    <div class="input-group top-header2">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Start and End appointment</span>
                        </div>
                        <input type="time" name="start" class="form-control">
                        <input type="time" name="end" class="form-control">
                        <input type="hidden" id="idDoctor" name="idDoctor">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-danger" value="MAKE AN APPOINTMENT">
                </div>
            </form>
        </div>
    </div>
</div>

<script src="js/main.js"></script>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/aos.js"></script>
<script src="js/main.js"></script>

<script src="js/main.js"></script>
<script>
    $(document).on("click", ".mClass", function () {
        var id = $(this).data('id');
        $("#idDoctor").val(id);
    })

</script>
<script>
    setTimeout(function () {
        $('.boxHidden').fadeOut('fast')
    }, 6000);
</script>
</body>
</html>