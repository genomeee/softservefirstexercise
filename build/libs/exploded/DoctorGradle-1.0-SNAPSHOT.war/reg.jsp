<%@ page import="models.User" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Registration</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <meta charset="UTF-8">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="css/main.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body style="background-image: url(images/hero_bg_1.jpg); width: 100%;height: 100%;background-size: 100%;">
<div class="site-wrap">

    <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->


    <div class="site-navbar-wrap" >
        <div class="site-navbar">
            <div class="container py-1">
                <div class="row align-items-center">
                    <div class="col-2">
                        <h2 class="mb-0 site-logo"><a href="login.jsp">DoctorServise</a></h2>
                    </div>
                    <div class="col-10">
                        <nav class="site-navigation text-right" role="navigation">
                            <div class="container">
                                <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>
                                <ul class="site-menu js-clone-nav d-none d-lg-block">
                                    <li><a href="aboutAs.jsp">About Us</a></li>
                                    <li><a href="<c:url value="/allDoctors"/>" >Doctors</a></li>
                                    <li class="has-children">
                                        <a href="patients.html">Log in</a>
                                        <ul class="dropdown arrow-top">
                                            <% if (session.getAttribute("User") == null ) {%>
                                            <li><a href ="<c:url value = "/login.jsp"/>" >Log in</a></li>
                                            <li><a href="<c:url value = "/reg.jsp"/>" >Registration</a></li>
                                            <%}%>
                                            <% User user = (User) session.getAttribute("User");
                                                if (user != null && user.getRole()!=null){%>
                                            <li class=""><a href="<c:url value="/logout"/>" class="nav-link">Log out</a>
                                                <b>${user.getRole().toString()} ${user.getFirstName()} ${user.getLastName()}</b></li>
                                            <%}%>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="wrapper wrapper--w780 top-header">
    <div class="card card-3">
        <div class="card-body">
            <h2 class="title">Registration Info</h2>
            <form method="POST" action="reg">
                <div class="input-group">
                    <input class="input--style-3" type="text" placeholder="First Name" id="firstName" name="firstName"  pattern="^[A-Z][a-z]{2,14}$" required>

                </div>
                <div class="input-group">
                    <input class="input--style-3" type="text" placeholder="Last Name" name="lastName"  pattern="^[A-Z][a-z]{2,14}$" required>
                </div>
                <div class="input-group">
                    <input class="input--style-3 js-datepicker" type="date" placeholder="Birthdate" name="birthday" id="dateBirth">
                    <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                </div>

                <div class="input-group">
                    <input class="input--style-3" type="email" placeholder="Email" name="email">
                </div>
                <div class="input-group">
                    <input class="input--style-3" type="text" placeholder="Phone" name="phone" pattern="^\+380\d{3}\d{2}\d{2}\d{2}$" required>
                </div>
                <div class="input-group">
                    <input class="input--style-3" type="password" placeholder="Password" name="password"  required>
                </div>
                <div class="input-group">
                    <input class="input--style-3" type="password" placeholder="Password" name="confirmPass" required>
                </div>
                <div class="p-t-10">
                    <button class="btn btn--pill btn--green" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="dropDownSelect1"></div>
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/select2/select2.min.js"></script>
<script src="js/global.js"></script>
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/select2/select2.min.js"></script>
<script src="js/main.js"></script>
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="js/validation.js"></script>
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/select2/select2.min.js"></script>
<script src="js/main.js"></script>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/aos.js"></script>
<script src="js/main.js"></script>

<script language="JavaScript" type="text/JavaScript">

    $(window).load(function(){
        document.getElementById("dateB").value = "${dateB}";
        var today = new Date();
        var min = new Date();
        var dd = today.getDate() - 1;
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        min = (yyyy - 120) + '-' + mm + '-' + dd;
        today = yyyy + '-' + mm + '-' + dd;
        document.getElementById("dateB").setAttribute("max", today);
        document.getElementById("dateB").setAttribute("min", min);
    });
</script>


</body>
</html>