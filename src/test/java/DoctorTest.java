import Services.VisitService;
import exeptions.AddRecordExeption;
import models.Doctor;
import models.Patient;
import models.TimeSlot;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.*;

public class DoctorTest {
    @DataProvider
    public Object[][] getAllFreeIntervalProvider(){
        List<TimeSlot> timeToRecords1 = new ArrayList<>();
        List<TimeSlot> timeToRecords2 = new ArrayList<>();
        List<TimeSlot> timeToRecords3 = new ArrayList<>();

        TimeSlot needTime1 = new TimeSlot(LocalTime.of(12, 20), LocalTime.of(14, 0));
        TimeSlot needTime2 = new TimeSlot(LocalTime.of(14, 0), LocalTime.of(15, 40));

        TimeSlot needTime3 = new TimeSlot(LocalTime.of(13, 0), LocalTime.of(14, 0));
        TimeSlot needTime4 = new TimeSlot(LocalTime.of(14, 0), LocalTime.of(15, 0));

        TimeSlot needTime5 = new TimeSlot(LocalTime.of(12, 45), LocalTime.of(13, 30));
        TimeSlot needTime6 = new TimeSlot(LocalTime.of(13, 30), LocalTime.of(14, 15));
        TimeSlot needTime7 = new TimeSlot(LocalTime.of(14, 15), LocalTime.of(15, 0));
        TimeSlot needTime8 = new TimeSlot(LocalTime.of(15, 0), LocalTime.of(15, 45));


        timeToRecords1.add(needTime1);
        timeToRecords1.add(needTime2);

        timeToRecords2.add(needTime3);
        timeToRecords2.add(needTime4);

        timeToRecords3.add(needTime5);
        timeToRecords3.add(needTime6);
        timeToRecords3.add(needTime7);
        timeToRecords3.add(needTime8);
        return new Object[][]{{100, timeToRecords1},{60, timeToRecords2}, {45, timeToRecords3}};

    }

    @DataProvider
    public Object[][] addToRecordsProvider(){
        return new Object[][]
                {{new TimeSlot(LocalTime.of(10, 30), LocalTime.of(11, 40)), true},
                {new TimeSlot(LocalTime.of(6, 30), LocalTime.of(7, 40)), false},
                {new TimeSlot(LocalTime.of(6, 30), LocalTime.of(7, 40)), false},
                {new TimeSlot(LocalTime.of(9, 30), LocalTime.of(11, 40)), true},
                {new TimeSlot(LocalTime.of(10, 30), LocalTime.of(15, 40)), true},
                {new TimeSlot(LocalTime.of(21, 30), LocalTime.of(22, 40)), false},
                {new TimeSlot(LocalTime.of(21, 30), LocalTime.of(21, 40)), true},
                {new TimeSlot(LocalTime.of(17, 30), LocalTime.of(18, 40)), true},
                {new TimeSlot(LocalTime.of(9, 30), LocalTime.of(9, 40)), true},
                {new TimeSlot(LocalTime.of(15, 30), LocalTime.of(15, 35)), true},
                {new TimeSlot(LocalTime.of(6, 30), LocalTime.of(22, 40)), false}};
    }
    @DataProvider
    public Object[][]getFirstIntervalProvider2() throws AddRecordExeption {

        Doctor doctor = new Doctor((long) 1, "Doctor", "Durnew", LocalTime.of(9, 0), LocalTime.of(20, 0));
        doctor.addToRecords(new TimeSlot(LocalTime.of(10, 30), LocalTime.of(11, 40)));
        doctor.addToRecords(new TimeSlot(LocalTime.of(16, 0), LocalTime.of(17, 0)));
        doctor.addToRecords(new TimeSlot(LocalTime.of(9, 30), LocalTime.of(9, 40)));

        LocalTime localTime = LocalTime.of(12,00);
        LocalTime localTime1 = LocalTime.of(17, 00);
        LocalTime localTime2 = LocalTime.of(11, 00);
        LocalTime localTime3 = LocalTime.of(20,00);


        return new Object[][]{{doctor, localTime,100, true}, {doctor,localTime1,60, true}, {doctor , localTime2 ,30, true}, {doctor, localTime3, 200, false}};
    }

    @DataProvider
    public Object[][]getFirstIntervalProvider3() throws AddRecordExeption {

        Doctor doctor = new Doctor((long) 1, "Doctor", "Durnew", LocalTime.of(9, 0), LocalTime.of(20, 0));
        doctor.addToRecords(new TimeSlot(LocalTime.of(10, 30), LocalTime.of(11, 40)));
        doctor.addToRecords(new TimeSlot(LocalTime.of(16, 0), LocalTime.of(17, 0)));
        doctor.addToRecords(new TimeSlot(LocalTime.of(9, 30), LocalTime.of(9, 40)));
        doctor.addToRecords(new TimeSlot(LocalTime.of(14, 10), LocalTime.of(14, 50)));

        TimeSlot needTime1 = new TimeSlot(LocalTime.of(12, 20), LocalTime.of(14, 0));
        TimeSlot needTime2 = new TimeSlot(LocalTime.of(14, 0), LocalTime.of(15, 40));

        TimeSlot needTime3 = new TimeSlot(LocalTime.of(13, 0), LocalTime.of(14, 0));
        TimeSlot needTime4 = new TimeSlot(LocalTime.of(14, 0), LocalTime.of(15, 0));


        return new Object[][]{{doctor, needTime1,100, true}, {doctor, needTime2,60, false}, {doctor , needTime3 ,30, false}, {doctor,needTime4,40,false}};
    }


    @DataProvider
    public Object[][] getRecordProvider() throws AddRecordExeption {

        Doctor doctor = new Doctor((long) 1, "Doctor", "Durnew", LocalTime.of(9, 0), LocalTime.of(20, 0));
        doctor.addToRecords(new TimeSlot(LocalTime.of(10, 30), LocalTime.of(11, 40)));
        doctor.addToRecords(new TimeSlot(LocalTime.of(16, 0), LocalTime.of(17, 0)));
        doctor.addToRecords(new TimeSlot(LocalTime.of(9, 30), LocalTime.of(9, 40)));
        doctor.addToRecords(new TimeSlot(LocalTime.of(14, 10), LocalTime.of(14, 50)));
        Patient patient = new Patient(1l, "Misha", "Sydora");
        Patient patient2 = new Patient(1l, "Ionela", "Lupashku");
        Patient patient3 = new Patient(1l, "Misha", "Sydora");
        Patient patient4 = new Patient(1l, "Misha", "Sydora");


        TimeSlot needTime = new TimeSlot(LocalTime.of(13, 0), LocalTime.of(14, 0));
        TimeSlot needTime2 = new TimeSlot(LocalTime.of(16, 0), LocalTime.of(16, 30));

        patient.setTimeSlot(needTime);
        patient2.setTimeSlot(needTime2);
        return new Object[][]{{doctor, patient, true},{doctor,patient2,false}};
    }

    @DataProvider
    public Object[][] getFirstFreeRecordProvider(){
        return new Object[][]{{30, true },
                {100, true},
                {20, true},
                {500,false}};
    }

    @Test(dataProvider = "addToRecordsProvider")
    public void testAddToRecordList(TimeSlot timeSlot, boolean f) {
            Doctor doctor = new Doctor((long) 1, "models.Doctor", "Durnew",
                    LocalTime.of(9, 0), LocalTime.of(22, 0));
        assertEquals(doctor.isCanInsert(timeSlot), f);
    }

    @Test(dataProvider = "getFirstFreeRecordProvider")
    public void testGetFirstRecord(int intervall, boolean f) throws AddRecordExeption {
            VisitService visitService = new VisitService();
            Doctor doctor = new Doctor((long) 1, "Doctor", "Durnew", LocalTime.of(9, 0),
                    LocalTime.of(22, 0));
            doctor.addToRecords(new TimeSlot(LocalTime.of(10, 30), LocalTime.of(11, 40)));
            doctor.addToRecords(new TimeSlot(LocalTime.of(16, 0), LocalTime.of(17, 0)));
            doctor.addToRecords(new TimeSlot(LocalTime.of(9, 30), LocalTime.of(9, 40)));
            assertEquals(visitService.patientSearchTime(doctor,intervall), f);
    }

    @Test(dataProvider = "getFirstIntervalProvider2")
    public void testGetFirstRecord2(Doctor doctor, LocalTime localTime, int intervall, boolean b) throws AddRecordExeption {
        VisitService visitService = new VisitService();
        assertEquals(visitService.patientSearchTime(doctor,localTime,intervall), b);
    }
    @Test(dataProvider = "getFirstIntervalProvider3")
    public void testGetFirstRecord3(Doctor doctor, TimeSlot timeSlot, int intervall, boolean b) throws AddRecordExeption {
        VisitService visitService = new VisitService();
        assertEquals(visitService.patientSearchTime(doctor, timeSlot,intervall), b);
    }

    @Test(dataProvider = "getAllFreeIntervalProvider", expectedExceptions = AddRecordExeption.class)
    public void testGetAllFreeIntervall(int intervall, List<TimeSlot> timeSlots) throws AddRecordExeption{
        VisitService visitService = new VisitService();
        Doctor doctor = new Doctor((long) 1, "Doctor", "Durnew", LocalTime.of(9, 0), LocalTime.of(22, 0));
        try {

            doctor.addToRecords(new TimeSlot(LocalTime.of(16, 0), LocalTime.of(17, 0)));
            doctor.addToRecords(new TimeSlot(LocalTime.of(15, 30), LocalTime.of(16, 45)));
            doctor.addToRecords(new TimeSlot(LocalTime.of(9, 30), LocalTime.of(10, 20)));
            doctor.addToRecords(new TimeSlot(LocalTime.of(19, 40), LocalTime.of(21, 50)));
            doctor.addToRecords(new TimeSlot(LocalTime.of(11, 0), LocalTime.of(12, 0)));
            doctor.addToRecords(new TimeSlot(LocalTime.of(18, 0), LocalTime.of(18, 15)));
            doctor.addToRecords(new TimeSlot(LocalTime.of(18, 30), LocalTime.of(19, 0)));
        }catch (AddRecordExeption addRecordExeption){
            throw new AddRecordExeption("Лікар зайнятий в цей час");
        }
        assertEquals(visitService.getAllFreeInterval(doctor,intervall), timeSlots);
    }

    @Test(dataProvider = "getRecordProvider")
    public void testGetRecord(Doctor doctor, Patient patient, boolean f) throws AddRecordExeption {
        VisitService visitService = new VisitService();
        assertEquals(visitService.patientGetTime(doctor,patient),f);}
}