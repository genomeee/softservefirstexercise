package Services;

import DAO.DoctorDAO;
import DAO.PatientDAO;
import DAO.TimeSlotDAO;
import DAO.UserDAO;
import encryption.BCrypter;
import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.Doctor;
import models.Patient;
import models.TimeSlot;
import org.apache.log4j.Logger;
import validator.AppoitmentsValidation;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class PatientService {

    PatientDAO patientDAO = new PatientDAO();
    private static final String USER_ALREADY_EXIST_MESSAGE = "User with this login already exist!";
    private static final String PASSWORDS_NOT_EQUAL = "Passwords are not equal!";
    private static Logger logger = Logger.getLogger(PatientService.class);
    public List<Patient> findAll() throws SQLException, ClassNotFoundException {
        List<Patient> all = patientDAO.findAll();
        return all;
    }

    public Patient findById(long id) throws SQLException, ClassNotFoundException, DependencyExeption {
        logger.info("Try find patient " + id );
        Patient patient = patientDAO.findById(id);
        return patient;
    }

    public void delete(Long id) throws SQLException, ClassNotFoundException, DependencyExeption {
        System.out.println(id);
        patientDAO.delete(id);
    }

    public void create(Patient patient) throws SQLException, ClassNotFoundException {
        patientDAO.save(patient);
    }

    public List<String> registerPatient(String firstName, String lastName, LocalDate date, String phone, String login,
                                        String password, String confirmPassword) throws SQLException, ClassNotFoundException {
        List<String> errorMessages = new ArrayList<>();

        if (!password.equals(confirmPassword)) {
            errorMessages.add(PASSWORDS_NOT_EQUAL);
            return errorMessages;
        }

        if (getUserFromDB(login)) {
            errorMessages.add(USER_ALREADY_EXIST_MESSAGE);
            return errorMessages;
        }

        Patient patient = new Patient();
        patient.setFirstName(firstName);
        patient.setLastName(lastName);
        patient.setEmail(login);
        patient.setBirthDate(date);
        patient.setPhone(phone);
        patient.setPassword(BCrypter.getHashPassword(password));
        errorMessages = new AppoitmentsValidation().validate(patient);

        if (errorMessages.isEmpty())
            new PatientDAO().save(patient);

        return errorMessages;
    }

    public List<TimeSlot> getAllPatientTS(Long id) throws SQLException, ClassNotFoundException, AddRecordExeption, DependencyExeption {
        List<TimeSlot> timeSlots = patientDAO.findAllPatientTimeSlots(id);
        return timeSlots;
    }

    public List<String> addTimeSlot(Long idPatient, Long idDoctor, LocalTime start, LocalTime end) throws SQLException, ClassNotFoundException, AddRecordExeption, DependencyExeption {
        List<String> errorMessages = new ArrayList<>();

        Doctor doctor=new DoctorDAO().findById(idDoctor);

        TimeSlot timeSlot = new TimeSlot(start,end);
        timeSlot.setDoctor(new DoctorDAO().findById(idDoctor));
        timeSlot.setPatient(new PatientDAO().findById(idPatient));
        errorMessages = new AppoitmentsValidation().validate(timeSlot);
        List<TimeSlot> timeSlots = new PatientDAO().findAllPatientTimeSlots(idPatient);
        if(errorMessages.isEmpty()) {
            if(doctor.isCanInsert(timeSlot)){
                if(new VisitService().addTimeSlotPatient(timeSlots,timeSlot))
                    new TimeSlotDAO().save(timeSlot);
                else {
                    errorMessages.add("You are busy");
                }
            }
            else {
                errorMessages.add("Doctor is busy");
            }
        }
        return errorMessages;
    }


    private boolean getUserFromDB(String email) throws SQLException, ClassNotFoundException {
        return new UserDAO().isUserInDB(email);

    }

    public List<String> deletePatient(Patient currentPatient, String currentPassword) throws SQLException,
            ClassNotFoundException, DependencyExeption {
        List<String> errors = new ArrayList<>();

        if (BCrypter.checkPassword(currentPassword, currentPatient.getPassword().trim())) {
            errors.add("Current password is wrong!");
            return errors;
        }

//        new UserDAO().deleteUserById(currentPatient.getId());
        new PatientDAO().delete(currentPatient.getId());
        return errors;
    }

    public void AdminDelete(Long id) throws SQLException, DependencyExeption, ClassNotFoundException {
        patientDAO.delete(id);
    }

    public List<String> updatePatientInfo(Patient currentPatient, String firstName, String lastName, String login, String phone, LocalDate date)
            throws SQLException, ClassNotFoundException {
        List<String> errors = new ArrayList<>();
        if(!currentPatient.getEmail().equals(login)) {
            if (getUserFromDB(login)) {
                errors.add(USER_ALREADY_EXIST_MESSAGE);
                return errors;
            }
        }
        currentPatient.setFirstName(firstName);
        currentPatient.setLastName(lastName);

        currentPatient.setBirthDate(date);
        currentPatient.setPhone(phone);
        currentPatient.setEmail(login);
        errors = new AppoitmentsValidation().validate(currentPatient);
        if (!errors.isEmpty())
            return errors;

        new PatientDAO().update(currentPatient);

        return errors;
    }
}
