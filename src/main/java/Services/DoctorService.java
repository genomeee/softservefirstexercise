package Services;

import DAO.DoctorDAO;
import DAO.UserDAO;
import encryption.BCrypter;
import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.Doctor;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import validator.AppoitmentsValidation;

import java.sql.SQLException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class DoctorService {

    private DoctorDAO doctorDAO = new DoctorDAO();
    private static final String USER_ALREADY_EXIST_MESSAGE = "User with this login already exist!";
    private static final String PASSWORDS_NOT_EQUAL = "Passwords are not equal!";
    private final Logger logger = LogManager.getLogger(DoctorService.class);

    public List<String> registerDoctor(String firstName, String lastName, LocalTime start_time, LocalTime end_time,
                                       String email, String password, String confirmPassword)
            throws SQLException, ClassNotFoundException {
        List<String> errorMessages = new ArrayList<>();
        logger.info("Register doctor");
        if (!password.equals(confirmPassword)) {
            logger.error(PASSWORDS_NOT_EQUAL);
            errorMessages.add(PASSWORDS_NOT_EQUAL);
            return errorMessages;
        }

        if (getUserFromDB(email)) {
            logger.error(USER_ALREADY_EXIST_MESSAGE);
            errorMessages.add(USER_ALREADY_EXIST_MESSAGE);
            return errorMessages;
        }

        Doctor doctor = new Doctor();
        doctor.setFirstName(firstName);
        doctor.setLastName(lastName);
        doctor.setEmail(email);
        doctor.setPassword(BCrypter.getHashPassword(password));
        doctor.setStartTime(start_time);
        doctor.setEndTime(end_time);
        logger.info("Doctor: " + doctor);

        errorMessages = new AppoitmentsValidation().validate(doctor);

        if (errorMessages.isEmpty()) {
            logger.info("Add doctor to DB");
            new DoctorDAO().save(doctor);
        }

        return errorMessages;
    }

    public List<Doctor> findAll() throws SQLException, ClassNotFoundException, AddRecordExeption {
        List<Doctor> all = doctorDAO.findAll();
        return all;
    }

    public Doctor findById(Long id) throws SQLException, ClassNotFoundException, AddRecordExeption {
        Doctor doctor = doctorDAO.findById(id);
        return doctor;
    }

    public void AdminDelete(Long id) throws SQLException, DependencyExeption, ClassNotFoundException {
        doctorDAO.delete(id);
    }

    public List<String> delete(Doctor currentDoctor, String currentPassword) throws SQLException,
            ClassNotFoundException, DependencyExeption {
        List<String> errors = new ArrayList<>();

        if (BCrypter.checkPassword(currentPassword, currentDoctor.getPassword().trim())) {
            errors.add("Current password is wrong!");
            return errors;
        }

        new DoctorDAO().delete(currentDoctor.getId());

        return errors;
    }


    public void create(Doctor doctor) throws SQLException, ClassNotFoundException {
        doctorDAO.save(doctor);
    }


    private boolean getUserFromDB(String email) throws SQLException, ClassNotFoundException {
        return new UserDAO().isUserInDB(email);
    }

    public List<String> updateDoctor(Doctor currentDoctor, String firstName, String lastName,
                                     String email, LocalTime startTime, LocalTime endTime) throws SQLException, ClassNotFoundException {
        List<String> errors = new ArrayList<>();
        if (!currentDoctor.getEmail().equals(email)) {
            if (getUserFromDB(email)) {
                errors.add(USER_ALREADY_EXIST_MESSAGE);
                return errors;
            }
        }
        currentDoctor.setFirstName(firstName);
        currentDoctor.setLastName(lastName);
        currentDoctor.setEmail(email);
        currentDoctor.setStartTime(startTime);
        currentDoctor.setEndTime(endTime);

        errors = new AppoitmentsValidation().validate(currentDoctor);
        if (!errors.isEmpty())
            return errors;

        new DoctorDAO().update(currentDoctor);

        return errors;
    }
}
