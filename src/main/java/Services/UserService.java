package Services;

import DAO.UserDAO;
import encryption.BCrypter;
import models.User;

import java.sql.SQLException;
import java.util.List;


public class UserService {
    private static final String LOGIN_ERROR_MESSAGE = "Either email or password is incorrect!";

    public String loginUserByPassword(String email, String password) throws SQLException, ClassNotFoundException {
        UserDAO userDAO = new UserDAO();
        User user = userDAO.getUserByEmail(email);
        if (user == null || BCrypter.checkPassword(password, user.getPassword().trim())){
            return LOGIN_ERROR_MESSAGE;
        }
        return null;
    }

    public User getUserFromDB(String email) throws SQLException, ClassNotFoundException {
        UserDAO userDAO = new UserDAO();
        System.out.println(userDAO.getUserByEmail(email));
        User user = userDAO.getUserByEmail(email);
        return user;
    }

    public List<User> findAll() throws SQLException, ClassNotFoundException {
        UserDAO userDAO = new UserDAO();
        List<User> all = userDAO.getAllUser();
        return all;
    }

    public boolean isRolePatient(String email) throws SQLException, ClassNotFoundException {
        User user = getUserFromDB(email);
        return user.getRole().isRolePatient();
    }
    public boolean isRoleAdmin(String email) throws SQLException, ClassNotFoundException {
        User user = getUserFromDB(email);
        return user.getRole().isRoleAdmin();
    }

    public boolean isRoleDoctor(String email) throws SQLException, ClassNotFoundException {
        User user = getUserFromDB(email);
        return user.getRole().isRoleDoctor();
    }
    public User getUserById(Long id) throws SQLException, ClassNotFoundException {
        return new UserDAO().getUserById(id);
    }


}
