package Services;

import DAO.TimeSlotDAO;
import encryption.BCrypter;
import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.Doctor;
import models.Patient;
import models.TimeSlot;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class TimeSlotService {
    private static Logger logger = Logger.getLogger(TimeSlotService.class);
    private TimeSlotDAO timeSlotDAO = new TimeSlotDAO();
    private static final int INTERVALL = 15;

    public void deleteTimeSlot(TimeSlot timeSlot) throws SQLException,
            ClassNotFoundException, DependencyExeption {
        timeSlotDAO.delete(timeSlot.getId());
    }

    public List<String> deleteTimeSlot(TimeSlot timeSlot, Patient patient, String currentPassword) throws SQLException,
            ClassNotFoundException {
        List<String> errors = new ArrayList<>();
        LocalTime localTime = LocalTime.now();
        long elapsedMinutes = Duration.between(localTime, timeSlot.getFrom()).toMinutes();
        logger.info("Differense is " + elapsedMinutes);
        if((elapsedMinutes < 15) && (localTime.isBefore(timeSlot.getFrom()))){
            errors.add("You can't delete time slot before 15 minutes");
            return errors;
        }

        if (BCrypter.checkPassword(currentPassword, patient.getPassword().trim())) {
            errors.add("Current password is wrong!");
            return errors;
        }
        new TimeSlotDAO().delete(timeSlot.getId());

        return errors;
    }

    public List<String> deleteTimeSlotDoctor(TimeSlot timeSlot, Doctor doctor, String currentPassword) throws SQLException,
            ClassNotFoundException {
        List<String> errors = new ArrayList<>();
        if (BCrypter.checkPassword(currentPassword, doctor.getPassword().trim())) {
            errors.add("Current password is wrong!");
            return errors;
        }
        new TimeSlotDAO().delete(timeSlot.getId());

        return errors;
    }

    public void adminDelete(Long id){
        try {
            timeSlotDAO.delete(id);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public TimeSlot findByid(Long id) throws ClassNotFoundException, SQLException, DependencyExeption, AddRecordExeption {
        logger.info("Try find timeslot" + id);
        return timeSlotDAO.findById(id);
    }

    public List<String> updateTimeSlotInfo(TimeSlot currentTimeSlot, LocalTime startTime, LocalTime endTime)
            throws SQLException, ClassNotFoundException, AddRecordExeption {
        List<String> errors = new ArrayList<>();
        LocalTime localTime = LocalTime.now();
        long elapsedMinutes = Duration.between(localTime, currentTimeSlot.getFrom()).toMinutes();
        logger.info("Differense is " + elapsedMinutes);
        if((elapsedMinutes < INTERVALL) && (localTime.isBefore(currentTimeSlot.getFrom()))){
            errors.add("You can't delete or update time slot before 15 minutes");
            return errors;
        }
        Doctor doctor = new DoctorService().findById( currentTimeSlot.getDoctor().getId());
        logger.info("doctor -" + doctor);
        currentTimeSlot.setFrom(startTime);
        currentTimeSlot.setTo(endTime);
        if(!doctor.isCanInsert(currentTimeSlot)){
            errors.add("Doctor busy in this time");
            return errors;
        }
        new TimeSlotDAO().update(currentTimeSlot);

        return errors;
    }

    public List<TimeSlot> findAll() throws SQLException, ClassNotFoundException, AddRecordExeption {
        logger.info("Try find all timeSlot");
        List<TimeSlot> all = timeSlotDAO.findAll();
        return all;
    }
}
