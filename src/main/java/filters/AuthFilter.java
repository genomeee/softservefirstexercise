package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = {"/logout","/deleteTimeSlot","/patient","/updateTimeSlot", "/addTimeSlot", "/DeleteDoctorController", "/DeletePatientController",
"/deleteTimeSlotDoctor", "/updatePatient"})
public class AuthFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpSession session = httpServletRequest.getSession();
        if(session == null || session.getAttribute("User") == null) {
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            httpServletResponse.sendRedirect("login.jsp");
        }
        else
            chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
