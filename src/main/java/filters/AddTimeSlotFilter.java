package filters;

import models.User;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = {"/addTimeSlot"})
public class AddTimeSlotFilter implements Filter {
    private static Logger logger = Logger.getLogger(AddTimeSlotFilter.class);


    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpSession session = httpServletRequest.getSession();
        User user = (User) session.getAttribute("User");
        logger.info(user);
        if(session == null || session.getAttribute("User") == null || user.getRole().isRoleDoctor()) {
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;

            request.setAttribute("errors", "Doctor can't add time slot");
            httpServletResponse.sendRedirect("errorsPage.jsp");
        } else
            chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}