package models;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.Objects;
@validator.TimeSlot
public class TimeSlot {
    private Long id;
    @NotNull(message = "Start time cannot be null")
    private LocalTime from;
    @NotNull(message = "End time cannot be null")
    private LocalTime to;
    @Valid
    private Doctor doctor;
    @Valid
    private Patient patient;

    public TimeSlot(LocalTime from, LocalTime to) {
//        if (to.isBefore(from)) {
//            throw new IllegalArgumentException("to can't be before from");
//        }
        this.from = from;
        this.to = to;
    }

    public TimeSlot() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalTime getFrom() {
        return from;
    }

    public void setFrom(LocalTime from) {
        this.from = from;
    }

    public LocalTime getTo() {
        return to;
    }

    public void setTo(LocalTime to) {
        this.to = to;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeSlot that = (TimeSlot) o;
        return Objects.equals(from, that.from) &&
                Objects.equals(to, that.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to);
    }

    @Override
    public String toString() {
        return
                "From=" + from +
                "To=" + to;
    }

    public static final Comparator<TimeSlot> COMPARE_BY_FROM = Comparator.comparing(TimeSlot::getFrom);

}
