package Servlet;

import Services.PatientService;
import exeptions.DependencyExeption;
import models.Patient;
import models.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@WebServlet(name = "PatientUpdateController", urlPatterns = {"/updatePatient"})
public class PatientUpdateController extends HttpServlet {
    private final Logger logger = LogManager.getLogger(PatientUpdateController.class);
    private PatientService patientService = new PatientService();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/patient").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String login = request.getParameter("email");
        LocalDate date = LocalDate.parse(request.getParameter("dataB"));
        String phone = request.getParameter("phone");
        List<String> errors;
        try {
            User user = (User) request.getSession().getAttribute("User");
            Patient patient = patientService.findById(user.getId());
            errors = patientService.updatePatientInfo(patient, firstName, lastName, login, phone, date);
            if (!errors.isEmpty()) {
                logger.error("Update error" + errors);
                request.setAttribute("error", errors);
                request.setAttribute("patient", patient);
                getServletContext().getRequestDispatcher("/patient").forward(request, response);
                return;
            }
            logger.info("Successful update patient");
            //response.getWriter().print("<script>var inst = $('[data-remodal-id=update]').remodal(); inst.destroy();</script>");
            request.getSession().setAttribute("User", new PatientService().findById(user.getId()));
            response.sendRedirect("/patient");
            logger.info("Successful redirect");

        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e.getMessage());
        } catch (DependencyExeption dependencyExeption) {
            dependencyExeption.printStackTrace();
        }
    }
}
