package Servlet;

import Services.DoctorService;
import exeptions.AddRecordExeption;
import models.Doctor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "AllDoctor", urlPatterns = {"/allDoctors"})
public class AllDoctor extends HttpServlet {
    private DoctorService doctorService = new DoctorService();


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Doctor> doctorList = doctorService.findAll();
            request.setAttribute("doctors",doctorList);

        } catch (SQLException | ClassNotFoundException | AddRecordExeption e) {
            e.printStackTrace();
        }
        getServletContext().getRequestDispatcher("/doctors.jsp").forward(request, response);
    }

}
