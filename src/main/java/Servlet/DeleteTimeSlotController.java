package Servlet;

import Services.PatientService;
import Services.TimeSlotService;
import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.Patient;
import models.TimeSlot;
import models.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "DeleteTimeSlotController", urlPatterns = {"/deleteTimeSlot"})
public class DeleteTimeSlotController extends HttpServlet {
    private static final String USER_SESSION_ATTR = "User";
    private final Logger logger = LogManager.getLogger(DeleteTimeSlotController.class);
    private TimeSlotService timeSlotService = new TimeSlotService();
    private PatientService patientService = new PatientService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        logger.info("Try to delete timeslot");
        String currentPass = request.getParameter("password");
        Long id = Long.parseLong(request.getParameter("idDoctor"));
        List<String> errors=null;
        try {
            User user = (User) request.getSession().getAttribute("User");
            TimeSlot timeSlot = timeSlotService.findByid(id);
            timeSlot.setId(id);
            Patient patient = patientService.findById(user.getId());
            logger.info("Try to delete timeSlot : " + timeSlot.toString());
            errors = timeSlotService.deleteTimeSlot(timeSlot,patient, currentPass);
            if (!errors.isEmpty()) {
                List<TimeSlot> timeSlots = patientService.getAllPatientTS(user.getId());
                logger.error("Change password error" + errors);
                request.setAttribute("errors", errors);
                request.setAttribute("patient", patient);
                request.setAttribute("allTimeSlots", timeSlots);
                getServletContext().getRequestDispatcher("/PatientPersonalPage.jsp").forward(request, response);
                return;
            }
            logger.info("Successful delete");
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath()+"/patient"));

        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e.getMessage());
        } catch (DependencyExeption | AddRecordExeption dependencyExeption) {
            dependencyExeption.printStackTrace();
    }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
