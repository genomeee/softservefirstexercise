package Servlet;

import Services.PatientService;
import Services.TimeSlotService;
import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.Patient;
import models.TimeSlot;
import models.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.List;

@WebServlet(name = "UpdateTimeSlotController", urlPatterns = {"/updateTimeSlot"})
public class UpdateTimeSlotController extends HttpServlet {
    private TimeSlotService timeSlotService = new TimeSlotService();
    private final Logger logger = LogManager.getLogger(UpdateTimeSlotController.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Long id =  Long.parseLong(request.getParameter("idTimeSlot"));
        LocalTime start = LocalTime.parse(request.getParameter("start"));
        LocalTime end = LocalTime.parse(request.getParameter("end"));
        List<String> errors;
        try {
            User user = (User) request.getSession().getAttribute("User");
            Patient patient = new PatientService().findById(user.getId());
            TimeSlot timeSlot =timeSlotService.findByid(id);
            timeSlot.setId(id);
            List<TimeSlot> timeSlots = new PatientService().getAllPatientTS(user.getId());
            errors = timeSlotService.updateTimeSlotInfo(timeSlot, start, end);
            if (!errors.isEmpty()) {

                request.setAttribute("allTimeSlots",timeSlots);
                logger.error("Update error" + errors);
                request.setAttribute("error", errors);
                request.setAttribute("patient", patient);
                getServletContext().getRequestDispatcher("/PatientPersonalPage.jsp").forward(request, response);
                return;
            }
            logger.info("Successful update patient");
            request.getSession().setAttribute("User", user);
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath()+"/patient"));
            logger.info("Successful redirect");

        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e.getMessage());
        } catch (DependencyExeption | AddRecordExeption dependencyExeption) {
            dependencyExeption.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
