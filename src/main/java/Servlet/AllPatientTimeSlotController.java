package Servlet;

import Services.PatientService;
import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.TimeSlot;
import models.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "AllPatientTimeSlotController", urlPatterns = {"/patient"})
public class AllPatientTimeSlotController extends HttpServlet {
    private final Logger logger = LogManager.getLogger(DeleteTimeSlotController.class);

    private PatientService patientService= new PatientService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            User user=(User) request.getSession().getAttribute("User");
            logger.info("cbcsbcsxdfgvhbjk,l"+ user.getId());
            List<TimeSlot> timeSlots = patientService.getAllPatientTS(user.getId());
            request.setAttribute("allTimeSlots",timeSlots);

        } catch (SQLException | ClassNotFoundException | AddRecordExeption | DependencyExeption e) {
            e.printStackTrace();
        }
        getServletContext().getRequestDispatcher("/PatientPersonalPage.jsp").forward(request, response);
    }
}
