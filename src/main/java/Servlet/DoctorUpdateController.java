package Servlet;

import Services.DoctorService;
import exeptions.AddRecordExeption;
import models.Doctor;
import models.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.List;

@WebServlet(name = "DoctorUpdateController", urlPatterns = {"/updateDoctor"})
public class DoctorUpdateController extends HttpServlet {
    private DoctorService doctorService = new DoctorService();
    private final Logger logger = LogManager.getLogger(DoctorUpdateController.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String login=request.getParameter("email");
        LocalTime start= LocalTime.parse(request.getParameter("start"));
        LocalTime end= LocalTime.parse(request.getParameter("end"));
        List<String> errors = null;
        try {
            User user = (User) request.getSession().getAttribute("User");
            Doctor doctor =  doctorService.findById(user.getId());
            errors =  doctorService.updateDoctor(doctor, firstName, lastName, login, start,end);
            if (!errors.isEmpty()) {
                logger.error("Update error" + errors);
                request.setAttribute("error", errors);
                request.setAttribute("doctor", doctor);
                getServletContext().getRequestDispatcher("/DoctorPersonalPage.jsp").forward(request, response);
                return;
            }
            logger.info("Successful update doctor");
            request.getSession().setAttribute("User", doctorService.findById(user.getId()));
            response.sendRedirect("DoctorPersonalPage.jsp");
            logger.info("Successful redirect");

        } catch (SQLException | AddRecordExeption  | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
