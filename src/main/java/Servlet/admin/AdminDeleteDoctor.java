package Servlet.admin;

import Services.DoctorService;
import Services.PatientService;
import Services.TimeSlotService;
import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.Doctor;
import models.Patient;
import models.TimeSlot;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "AdminDeleteDoctor", urlPatterns = {"/adminDelete"})
public class AdminDeleteDoctor extends HttpServlet {
    private final Logger logger = LogManager.getLogger( AdminDeleteDoctor.class);
    private DoctorService doctorService = new DoctorService();
    private PatientService patientService = new PatientService();
    private TimeSlotService timeSlotService = new TimeSlotService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            response.setContentType("text/html;charset=UTF-8");
            Long id = Long.parseLong(request.getParameter("idDoctor"));
            logger.info("Try to delete doctor");
            List<Doctor> doctorList = null;
            List<Patient> patientList = null;
            List<TimeSlot> timeSlots = null;
            try {
                doctorList = doctorService.findAll();
                patientList = patientService.findAll() ;
                timeSlots = timeSlotService.findAll();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (AddRecordExeption addRecordExeption) {
                addRecordExeption.printStackTrace();
            }
            request.setAttribute("doctors",doctorList);
            request.setAttribute("patients", patientList);
            request.setAttribute("timeSlots", timeSlots);
            try {
                Doctor doctor = doctorService.findById(id);
                logger.info("Try to delete doctor : "+ doctor.toString());
                doctorService.AdminDelete(doctor.getId());
                logger.info("Successful delete");
                request.setAttribute("success", "Successful delete");
                response.sendRedirect(response.encodeRedirectURL(request.getContextPath()+"/admin"));

            } catch (SQLException | ClassNotFoundException e) {
                logger.error(e.getMessage());
                request.setAttribute("errors", e.getMessage());
                request.getRequestDispatcher("adminPage.jsp").forward(request, response);
            } catch (DependencyExeption | AddRecordExeption dependencyExeption) {
                dependencyExeption.printStackTrace();
            }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
