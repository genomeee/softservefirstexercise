package Servlet.admin;

import Services.DoctorService;
import Services.PatientService;
import Services.TimeSlotService;
import exeptions.AddRecordExeption;
import models.Doctor;
import models.Patient;
import models.TimeSlot;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "AdminServlet", urlPatterns = {"/admin"})
public class AdminServlet extends HttpServlet {
    private DoctorService doctorService = new DoctorService();
    private PatientService patientService = new PatientService();
    private TimeSlotService timeSlotService = new TimeSlotService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Doctor> doctorList = null;
        List<Patient> patientList = null;
        List<TimeSlot> timeSlots = null;
        try {
            doctorList = doctorService.findAll();
            patientList = patientService.findAll();
            timeSlots = timeSlotService.findAll();
        } catch (SQLException | ClassNotFoundException | AddRecordExeption e) {
            e.printStackTrace();
        }
        request.setAttribute("doctors",doctorList);
        request.setAttribute("patients", patientList);
        request.setAttribute("timeSlots", timeSlots);
        getServletContext().getRequestDispatcher("/adminPage.jsp").forward(request, response);
    }
}
