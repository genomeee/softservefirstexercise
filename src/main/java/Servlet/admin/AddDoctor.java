package Servlet.admin;

import Services.DoctorService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.List;

@WebServlet(name = "AddDoctor",urlPatterns = {"/addDoctor"})
public class AddDoctor extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String login=request.getParameter("email");

        String password=request.getParameter("password");
        String confirmPassword=request.getParameter("confirmPass");

        LocalTime start= LocalTime.parse(request.getParameter("start"));
        LocalTime end= LocalTime.parse(request.getParameter("end"));
        List<String> errors;
        try {
            errors = new DoctorService().registerDoctor(firstName,lastName,start,end,login,password,confirmPassword);
            if(!errors.isEmpty()) {
                request.setAttribute("registerError", errors);
                getServletContext().getRequestDispatcher("login.jsp").forward(request, response);
                return;
            }
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath() +"/admin"));
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
