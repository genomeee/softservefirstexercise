package Servlet;

import DAO.UserDAO;
import Services.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "login", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    static final String LOGIN_ERROR_KEY = "loginError";
    public static final String USER_SESSION_ATTR = "User";




    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/").forward(httpServletRequest, httpServletResponse);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserService userService = new UserService();

        String email = request.getParameter("email");
        String pass = request.getParameter("pass");
        String loginError;
        try {
            loginError = userService.loginUserByPassword(email,pass);
            if (loginError != null){
                request.setAttribute("loginError", loginError);
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }else if(userService.isRolePatient(email)){
                HttpSession session = request.getSession();
                session.setAttribute(USER_SESSION_ATTR, new UserDAO().getUserByEmail(email));
                session.setMaxInactiveInterval(300);
                response.sendRedirect(response.encodeRedirectURL(request.getContextPath()+"/patient"));
            }else if(userService.isRoleDoctor(email)){
                HttpSession session = request.getSession();
                session.setAttribute(USER_SESSION_ATTR, new UserDAO().getUserByEmail(email));
                session.setMaxInactiveInterval(300);
                response.sendRedirect("DoctorPersonalPage.jsp");
            }else if(userService.isRoleAdmin(email)) {
                HttpSession session = request.getSession();
                session.setAttribute(USER_SESSION_ATTR, new UserDAO().getUserByEmail(email));
                session.setMaxInactiveInterval(300);
                response.sendRedirect(response.encodeRedirectURL(request.getContextPath() +"/admin"));
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}