package Servlet;

import Services.DoctorService;
import Services.PatientService;
import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.Doctor;
import models.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.List;


@WebServlet(name = "addTimeSlot",urlPatterns = {"/addTimeSlot"})
public class AddTimeSlotController extends HttpServlet {
    private final Logger logger = LogManager.getLogger(AddTimeSlotController.class);
    private PatientService patientService=new PatientService();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        logger.info("Try to add record");
        Long id = Long.parseLong(request.getParameter("idDoctor"));
        LocalTime start=LocalTime.parse(request.getParameter("start"));
        LocalTime end=LocalTime.parse(request.getParameter("end"));
        List<Doctor> doctorList = null;
        try {
            doctorList = new DoctorService().findAll();
        } catch (SQLException | ClassNotFoundException | AddRecordExeption e) {
            e.printStackTrace();
        }
        request.setAttribute("doctors",doctorList);

        List<String> errors;
        try {
            User user=(User) request.getSession().getAttribute("User");
            logger.info("Try to add record for : "+user.getId());
            errors = patientService.addTimeSlot(user.getId(),id,start,end);
            if(!errors.isEmpty()) {
                logger.error("Add record error"+errors);
                request.setAttribute("errors", errors);

                request.getRequestDispatcher("doctors.jsp").forward(request, response);
                return;
            }
            errors.add("Successful add timeSlot");
            request.setAttribute("success", errors);
            request.getRequestDispatcher("doctors.jsp").forward(request, response);
            logger.info("Successful add timeSlot");


        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e.getMessage());

        } catch (AddRecordExeption | DependencyExeption addRecordExeption) {
            addRecordExeption.printStackTrace();
        }
    }
}
