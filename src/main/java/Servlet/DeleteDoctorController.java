package Servlet;

import Services.DoctorService;
import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.Doctor;
import models.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "DeleteDoctorController", urlPatterns = "/DeleteDoctorController")
public class DeleteDoctorController extends HttpServlet {
    private final Logger logger = LogManager.getLogger(DeletePatientController.class);
    private DoctorService doctorService = new DoctorService();
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        logger.info("Try to delete doctor");
        String currentPass = request.getParameter("password");
        List<String> errors=null;
        try {
            User user=(User) request.getSession().getAttribute("User");
            Doctor doctor = doctorService.findById(user.getId());
            logger.info("Try to delete patient : "+ doctor.toString());
            errors = doctorService.delete(doctor,currentPass);
            if(!errors.isEmpty()) {
                logger.error("Change password error"+errors);
                request.setAttribute("errors", errors);
                request.setAttribute("patient", doctor);
                getServletContext().getRequestDispatcher("PatientPersonalPage.jsp").forward(request, response);
                return;
            }
            logger.info("Successful delete");

            request.getSession().invalidate();
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath()+"/login"));

        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e.getMessage());
        } catch (DependencyExeption | AddRecordExeption dependencyExeption) {
            dependencyExeption.printStackTrace();
        }
    }

}
