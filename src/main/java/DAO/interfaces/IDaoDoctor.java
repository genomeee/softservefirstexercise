package DAO.interfaces;

import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.Doctor;

import java.sql.SQLException;
import java.util.List;

public interface IDaoDoctor {
    Doctor findById(Long i) throws SQLException, ClassNotFoundException, AddRecordExeption;

    List<Doctor> findAll() throws SQLException, ClassNotFoundException, AddRecordExeption;

    void delete(Long id) throws SQLException, ClassNotFoundException, DependencyExeption;

    void save(Doctor doctor) throws SQLException, ClassNotFoundException;

    void update(Doctor doctor) throws SQLException, ClassNotFoundException;
}
