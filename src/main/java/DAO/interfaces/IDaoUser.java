package DAO.interfaces;

import exeptions.DependencyExeption;
import models.User;

import java.sql.SQLException;
import java.util.List;

public interface IDaoUser {
    User getUserById(Long id) throws ClassNotFoundException, SQLException;

    User getUserByEmail(String login) throws ClassNotFoundException, SQLException;

    boolean isUserInDB(String login) throws SQLException, ClassNotFoundException;

    Long addUser(User user) throws ClassNotFoundException, SQLException;

    void deleteUserById(long id) throws SQLException, ClassNotFoundException, DependencyExeption;

    void updateUserById(User user) throws ClassNotFoundException, SQLException;

    List<User> getAllUser() throws SQLException, ClassNotFoundException;

}
