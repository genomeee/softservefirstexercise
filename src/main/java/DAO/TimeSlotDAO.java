package DAO;

import DAO.interfaces.IDaoTimeSlot;
import DB.DataBaseConnection;
import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.TimeSlot;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TimeSlotDAO implements IDaoTimeSlot {
    private static Logger logger = Logger.getLogger(TimeSlotDAO.class);

    private static final String ID_COLUMN_NAME = "id";
    private static final String FROMTIME_COLUMN = "start_time";
    private static final String TOTIME_COLUMN = "end_time";
    private static final String DOCTOR_COLUMN = "id_doctor";
    private static final String PATIENT_COLUMN = "id_patient";


    @Override
    public void save(TimeSlot timeSlot) throws SQLException, ClassNotFoundException {
        Connection conn = DataBaseConnection.getConnection();
        logger.info("trying save time slot" + timeSlot);
        String sql =
                "INSERT INTO timeslots (start_time, end_time ,id_doctor, id_patient) " +
                        "VALUES (" +
                        "'" + timeSlot.getFrom() + "', " +
                        "'" + timeSlot.getTo() + "', " +
                        "'" + timeSlot.getDoctor().getId() + "', " +
                        "'" + timeSlot.getPatient().getId() + "'" +
                        ");";
        PreparedStatement st = conn.prepareStatement(sql);
        st.executeUpdate();

    }

    @Override
    public void update(TimeSlot timeSlot) throws SQLException, ClassNotFoundException {
        logger.info("Try to update record by id "+timeSlot.toString());
        Connection conn = DataBaseConnection.getConnection();
        String prepUpdate = "UPDATE timeslots SET id_doctor=?,id_patient =?,start_time=?,end_time=?" +
                " WHERE id=?;";
        PreparedStatement pr1 = conn.prepareStatement(prepUpdate);
        pr1.setLong(1, timeSlot.getDoctor().getId());
        pr1.setLong(2, timeSlot.getPatient().getId());
        pr1.setTime(3, Time.valueOf(timeSlot.getFrom()));
        pr1.setTime(4, Time.valueOf(timeSlot.getTo()));
        pr1.setLong(5, timeSlot.getId());
        int res = pr1.executeUpdate();
        if (res == 0){
            logger.error(timeSlot.toString()+" not found");
            throw new IllegalArgumentException(timeSlot.toString()+" not found");
        }
        logger.info("Updated : "+timeSlot.toString());
    }

    @Override
    public void delete(Long id) throws SQLException, ClassNotFoundException, IllegalArgumentException {
        logger.info("Try to delete timeslot by id "+id);
        Connection conn = DataBaseConnection.getConnection();
        String prepDelete1 = "DELETE FROM timeslots WHERE id= ?;";
        PreparedStatement pr1 = conn.prepareStatement(prepDelete1);
        pr1.setObject(1, id);
        int rs=pr1.executeUpdate();
//        conn.close();
        if (rs == 0) {
            logger.error("Timeslot "+id+" not found");
            throw new IllegalArgumentException("Record "+id+" not found");
        }
        logger.info("Deleted record: "+id);
    }



    @Override
    public TimeSlot findById(Long id) throws SQLException, ClassNotFoundException, DependencyExeption, AddRecordExeption {
        Connection conn = DataBaseConnection.getConnection();
        String prepQuerry1 = "SELECT * FROM timeslots WHERE id = ? ";
        logger.info("trying find time slot" + id);
        PreparedStatement pr1 = conn.prepareStatement(prepQuerry1);
        pr1.setLong(1, id);
        ResultSet rs = pr1.executeQuery();
        TimeSlot timeSlot = new TimeSlot();
        if (rs.next()) {
            timeSlot = new TimeSlot();
            timeSlot.setFrom(rs.getTime(FROMTIME_COLUMN).toLocalTime());
            timeSlot.setTo(rs.getTime(TOTIME_COLUMN).toLocalTime());
            timeSlot.setPatient(new PatientDAO().findById(rs.getLong("id_patient")));
            timeSlot.setDoctor(new DoctorDAO().findById(rs.getLong("id_doctor")));
            System.out.println(timeSlot.getDoctor().getId());
        }
        return timeSlot;
    }

    @Override
    public List<TimeSlot> findAll() throws SQLException, ClassNotFoundException {
        List<TimeSlot> timeSlots = new ArrayList<>();
        String sql = "SELECT * FROM timeslots";
        logger.info("trying find all time slots");
        Connection connection;
        connection = DataBaseConnection.getConnection();
        TimeSlot timeSlot = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                timeSlot = new TimeSlot();
                timeSlot.setId(resultSet.getLong(ID_COLUMN_NAME));
                timeSlot.setFrom(resultSet.getTime(FROMTIME_COLUMN).toLocalTime());
                timeSlot.setTo(resultSet.getTime(TOTIME_COLUMN).toLocalTime());
                timeSlot.setPatient(new PatientDAO().findById(resultSet.getLong("id_patient")));
                timeSlot.setDoctor(new DoctorDAO().findById(resultSet.getLong("id_doctor")));
                timeSlots.add(timeSlot);
//
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (DependencyExeption dependencyExeption) {
            dependencyExeption.printStackTrace();
        } catch (AddRecordExeption addRecordExeption) {
            addRecordExeption.printStackTrace();
        }
        logger.info("find " + timeSlots);
        return timeSlots;
    }

//    public static List<TimeSlot> findAllEmptySlots() throws SQLException, ClassNotFoundException {
//        List<TimeSlot> timeSlots = new ArrayList<>();
//        String sql = "select * from timeslots where (" + DOCTOR_COLUMN + "," + PATIENT_COLUMN + " ) is null";
//        Connection connection = DataBaseConnection.getConnection();
//        TimeSlot timeSlot = null;
//        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
//            ResultSet resultSet = preparedStatement.executeQuery();
//            while (resultSet.next()) {
//                timeSlot = new TimeSlot();
//                timeSlot.setFrom(resultSet.getTime(FROMTIME_COLUMN).toLocalTime());
//                timeSlot.setTo(resultSet.getTime(TOTIME_COLUMN).toLocalTime());
//                timeSlot.setDoctor(resultSet.getLong(DOCTOR_COLUMN));
//                timeSlot.setPatient(resultSet.getLong(PATIENT_COLUMN));
//                timeSlots.add(timeSlot);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return timeSlots;
//    }

    public static List<TimeSlot> findByDoctorId(long id) throws SQLException, ClassNotFoundException {
        logger.info("trying find all time slots for doctor" + id);
        List<TimeSlot> timeSlots = new ArrayList<>();
        String sql = "SELECT * FROM timeslots WHERE id_doctor=" + id + ";";
        Connection connection = DataBaseConnection.getConnection();
        TimeSlot timeSlot = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                timeSlot = new TimeSlot();
                timeSlot.setId(resultSet.getLong(ID_COLUMN_NAME));
                timeSlot.setFrom(resultSet.getTime(FROMTIME_COLUMN).toLocalTime());
                timeSlot.setTo(resultSet.getTime(TOTIME_COLUMN).toLocalTime());
                timeSlot.setPatient(new PatientDAO().findById(resultSet.getLong("id_patient")));
                timeSlot.setDoctor(new DoctorDAO().getById(resultSet.getLong("id_doctor")));
                timeSlots.add(timeSlot);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (DependencyExeption dependencyExeption) {
            dependencyExeption.printStackTrace();
        }

        return timeSlots;

    }



}
