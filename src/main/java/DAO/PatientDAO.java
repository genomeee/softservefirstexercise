package DAO;

import DAO.interfaces.IDaoPatient;
import DB.DataBaseConnection;
import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.Patient;
import models.Role;
import models.TimeSlot;
import models.User;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PatientDAO implements IDaoPatient {
    private static final String ID_COLUMN_NAME = "id";
    private static final String FIRSTNAME_COLUMN = "first_name";
    private static final String LASTNAME_COLUMN = "last_name";
    private static final String DATAB_COLUMN = "date_of_birth";
    private static final String PHONE = "phone";

    private static Logger logger = Logger.getLogger(PatientDAO.class);

    public Patient getPatientByLogin(String login) throws ClassNotFoundException, SQLException, DependencyExeption {
        logger.info("Trying find patient with login = " + login );
        User user=new UserDAO().getUserByEmail(login);
        Patient patient=null;
        if (user!=null){
            patient= findById(user.getId());

        }
        logger.info("find patient: " + patient);
        return patient;
    }

    @Override
    public void save(Patient patient) throws SQLException, ClassNotFoundException {
        if(patient.getId()==null) {
            logger.info("trying save patient" + patient);
            Connection conn = DataBaseConnection.getConnection();
            User user=new User();
            user.setId(patient.getId());
            user.setEmail(patient.getEmail());
            user.setPassword(patient.getPassword());
            user.setRole(new Role());
            Long id =new  UserDAO().addUser(user);
            String preparedQuerry1 = "INSERT INTO patients (id,first_name,last_name,date_of_birth," +
                    "phone) VALUES(?,?,?,?,?)";
            PreparedStatement preparedStmt = conn.prepareStatement(preparedQuerry1);
            System.out.println(id);
            preparedStmt.setLong(1, id);
            preparedStmt.setString(2, patient.getFirstName());
            preparedStmt.setString(3, patient.getLastName());
            preparedStmt.setDate(4,new Date(patient.getBirthDate().getYear()-1900,
                    patient.getBirthDate().getMonthValue() - 1, patient.getBirthDate().getDayOfMonth()));
            preparedStmt.setString(5, patient.getPhone());
            preparedStmt.executeUpdate();
        }
    }

    @Override
    public void update(Patient patient) throws SQLException, ClassNotFoundException, IllegalArgumentException {

        Connection conn = DataBaseConnection.getConnection();
        String prepUpdate = "UPDATE patients SET first_name=?,last_name =?,date_of_birth=?,phone=?" +
                " WHERE id=?;";
        logger.info("trying update patient" + patient);
        PreparedStatement pr1 = conn.prepareStatement(prepUpdate);
        pr1.setString(1, patient.getFirstName());
        pr1.setString(2, patient.getLastName());
        pr1.setDate(3,new Date(patient.getBirthDate().getYear()-1900,
                patient.getBirthDate().getMonthValue() - 1, patient.getBirthDate().getDayOfMonth()));
        pr1.setString(4,patient.getPhone());
        pr1.setObject(5, patient.getId());
        int res = pr1.executeUpdate();

        String prepUpdate2 = "UPDATE users SET email=?,password =?" +
                " WHERE id=?;";

        PreparedStatement pr2 = conn.prepareStatement(prepUpdate2);
        pr2.setString(1, patient.getEmail());
        pr2.setString(2, patient.getPassword());
        pr2.setObject(3, patient.getId());
        int res2 = pr2.executeUpdate();
        if (res == 0){
            logger.info("not found" + patient);
            throw new IllegalArgumentException(patient.toString()+" not found");
        }

    }

    @Override
    public Patient findById(Long id) throws SQLException, ClassNotFoundException, DependencyExeption {
        Connection conn = DataBaseConnection.getConnection();
        logger.info("trying find patient with id" + id);
        String prepQuerry1 = "SELECT * FROM patients WHERE id = ? ";
        PreparedStatement pr1 = conn.prepareStatement(prepQuerry1);
        pr1.setObject(1, id);
        ResultSet rs1 = pr1.executeQuery();
        Patient patient=null;
        if (rs1.next()) {
            patient=new Patient();
            patient.setId(rs1.getLong(ID_COLUMN_NAME));
            patient.setFirstName(rs1.getString("first_name"));
            patient.setLastName(rs1.getString("last_name"));
            patient.setBirthDate(rs1.getDate("date_of_birth").toLocalDate());
            patient.setPhone(rs1.getString("phone"));
            User user=new UserDAO().getUserById(id);
            patient.setEmail(user.getEmail());
            patient.setPassword(user.getPassword());
        }
        logger.info("find patient " + patient);
        return patient;

    }


    @Override
    public List<Patient> findAll() throws SQLException, ClassNotFoundException {
        logger.info("trying find all patient");
        List<Patient> patients = new ArrayList<>();
        Connection conn = DataBaseConnection.getConnection();
        String prepQuerry1 = "SELECT * FROM patients ";
        PreparedStatement pr1 = conn.prepareStatement(prepQuerry1);
        ResultSet resultSet= pr1.executeQuery();
        while (resultSet.next()) {
            Patient patient=new Patient();
            patient.setId(resultSet.getLong(ID_COLUMN_NAME));
            patient.setFirstName(resultSet.getString(FIRSTNAME_COLUMN));
            patient.setLastName(resultSet.getString(LASTNAME_COLUMN));
            patient.setBirthDate(resultSet.getDate(DATAB_COLUMN).toLocalDate());
            patient.setPhone(resultSet.getString(PHONE));
            User user=new UserDAO().getUserById(patient.getId());
            patient.setEmail(user.getEmail());
            patient.setPassword(user.getPassword());

            patients.add(patient);
        }
        logger.info("find all patient" + patients);
        return patients;
    }

    @Override
    public void delete(Long id) throws SQLException, ClassNotFoundException, IllegalArgumentException, DependencyExeption {
        logger.info("trying delete patient: " + id);
        Connection conn = DataBaseConnection.getConnection();
        String prepDelete1 = "DELETE FROM patients WHERE id= ?;";
        String prepDelete2 = "DELETE FROM users WHERE id= ?;";
        String prepQuery = "SELECT id FROM timeslots WHERE id_patient = ?;";
        PreparedStatement pr1 = conn.prepareStatement(prepDelete1);
        pr1.setObject(1, id);

        PreparedStatement pr3 = conn.prepareStatement(prepDelete2);
        pr3.setObject(1, id);

        PreparedStatement pr2 = conn.prepareStatement(prepQuery);
        pr2.setObject(1, id);
        ResultSet r = pr2.executeQuery();
        if (r.getFetchSize() != 0) {
            logger.info("patient has timeslots  " + id);
            throw new DependencyExeption("Patient "+id+" has timeslots");

        }
        int rs=pr1.executeUpdate();
        int rs2 = pr3.executeUpdate();

        if (rs == 0) {
            logger.warn("Patient not found:  " + id);
            throw new IllegalArgumentException("Patient "+id+" not found");
        }
    }

    public List<TimeSlot> findAllPatientTimeSlots(Long id) throws SQLException, ClassNotFoundException, DependencyExeption, AddRecordExeption {
        List<TimeSlot> timeSlots = new ArrayList<>();
        logger.info("trying find all patient time slot: " + id);
        Connection connection = DataBaseConnection.getConnection();
//        SELECT timeslots.id,id_doctor,doctors.last_name,
//                id_patient, timeslots.start_time, timeslots.end_time FROM patients, timeslots,
//        doctors WHERE timeslots.id_patient = 22 AND timeslots.id_doctor = doctors.id ORDER BY timeslots.start_time;
        String sql = "SELECT timeslots.id,id_doctor, id_patient, start_time, end_time " +
                "FROM timeslots WHERE timeslots.id_patient  = ? ORDER BY start_time";
        PreparedStatement pr1 = connection.prepareStatement(sql);
        pr1.setLong(1, id);
        ResultSet resultSet = pr1.executeQuery();
        while (resultSet.next()){
            TimeSlot timeSlot = new TimeSlot();
            timeSlot.setId(resultSet.getLong("id"));
            timeSlot.setFrom(resultSet.getTime("start_time").toLocalTime());
            timeSlot.setTo(resultSet.getTime("end_time").toLocalTime());
            timeSlot.setPatient(new PatientDAO().findById(resultSet.getLong("id_patient")));
            timeSlot.setDoctor(new DoctorDAO().getById(resultSet.getLong("id_doctor")));
            timeSlots.add(timeSlot);
            System.out.println(timeSlot);
        }
        logger.info("trying find all patient time slot: " + timeSlots);
        return timeSlots;
    }

}
