package DAO;

import DAO.interfaces.IDaoDoctor;
import DB.DataBaseConnection;
import Services.VisitService;
import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.Doctor;
import models.Role;
import models.TimeSlot;
import models.User;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DoctorDAO implements IDaoDoctor {
    private static Logger logger = Logger.getLogger(DoctorDAO.class);

    private static final String ID_COLUMN_NAME = "id";
    private static final String FIRSTNAME_COLUMN = "first_name";
    private static final String LASTNAME_COLUMN = "last_name";
    private static final String START_TIME_COLUMN = "start_time";
    private static final String END_TIME_COLUMN = "end_time";

    @Override
    public List<Doctor> findAll() throws SQLException, ClassNotFoundException, AddRecordExeption {
        List<Doctor> doctors = new ArrayList<>();
        Connection conn = DataBaseConnection.getConnection();
        String prepQuerry1 = "SELECT * FROM doctors ";
        logger.info("trying fing all doctors");
        PreparedStatement pr1 = conn.prepareStatement(prepQuerry1);
        ResultSet resultSet = pr1.executeQuery();
        while (resultSet.next()) {
            Doctor doctor = new Doctor();
            doctor.setId(resultSet.getLong(ID_COLUMN_NAME));
            doctor.setFirstName(resultSet.getString(FIRSTNAME_COLUMN));
            doctor.setLastName(resultSet.getString(LASTNAME_COLUMN));
            doctor.setStartTime(resultSet.getTime(START_TIME_COLUMN).toLocalTime());
            doctor.setEndTime(resultSet.getTime(END_TIME_COLUMN).toLocalTime());
            List<TimeSlot> timeSlots = new TimeSlotDAO().findByDoctorId(doctor.getId());
            for (TimeSlot timeSlot : timeSlots) {
                new VisitService().addTimeSlot(doctor, timeSlot);
            }
            User user=new UserDAO().getUserById(doctor.getId());
            doctor.setEmail(user.getEmail());
            doctor.setPassword(user.getPassword());
            doctors.add(doctor);
        }
        logger.info("all doctors: " + doctors);
        return doctors;
    }


    public Doctor getById(Long id) throws SQLException, ClassNotFoundException {
        Connection conn = DataBaseConnection.getConnection();
        String prepQuerry1 = "SELECT * FROM doctors WHERE id = ? ";
        logger.info("trying find doctor with id" + id);
        PreparedStatement pr1 = conn.prepareStatement(prepQuerry1);
        pr1.setLong(1, id);
        ResultSet rs = pr1.executeQuery();
        Doctor doctor=null;
        if (rs.next()) {
            doctor = new Doctor();
            doctor.setId(rs.getLong(ID_COLUMN_NAME));
            doctor.setFirstName(rs.getString(FIRSTNAME_COLUMN));
            doctor.setLastName(rs.getString(LASTNAME_COLUMN));
            doctor.setStartTime(rs.getTime(START_TIME_COLUMN).toLocalTime());
            doctor.setEndTime(rs.getTime(END_TIME_COLUMN).toLocalTime());
        }
        logger.info("Find doctor " + doctor);
        return doctor;
    }
    @Override
    public Doctor findById(Long i) throws SQLException, ClassNotFoundException, AddRecordExeption {
        Connection conn = DataBaseConnection.getConnection();;
        String sql = "SELECT * FROM doctors WHERE id=" + i + ";";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        logger.info("DoctorDAO findByid started, id = " + i);
        ResultSet rs = preparedStatement.executeQuery();
        Doctor doctor = null;
        if (rs.next()) {
            doctor = new Doctor();
            doctor.setId(rs.getLong(ID_COLUMN_NAME));
            doctor.setFirstName(rs.getString(FIRSTNAME_COLUMN));
            doctor.setLastName(rs.getString(LASTNAME_COLUMN));
            doctor.setStartTime(rs.getTime(START_TIME_COLUMN).toLocalTime());
            doctor.setEndTime(rs.getTime(END_TIME_COLUMN).toLocalTime());
            List<TimeSlot> timeSlots = new TimeSlotDAO().findByDoctorId(doctor.getId());
            for (TimeSlot timeSlot : timeSlots) {
                new VisitService().addTimeSlot(doctor, timeSlot);
            }
            User user=new UserDAO().getUserById(i);
            doctor.setEmail(user.getEmail());
            doctor.setPassword(user.getPassword());
        }
        logger.info("Find doctor " + doctor);
        return doctor;
    }

    public Doctor getDoctorByEmail(String email) throws ClassNotFoundException, SQLException,AddRecordExeption {
        User user=new UserDAO().getUserByEmail(email);
        Doctor doctor=null;
        if (user!=null){
            doctor= findById(user.getId()); }
        return doctor;
    }

    @Override
    public void delete(Long id) throws SQLException, ClassNotFoundException, DependencyExeption {
        logger.info("DoctorDAO try delete, id = " + id);
        Connection conn = DataBaseConnection.getConnection();
        String prepDelete1 = "DELETE FROM doctors WHERE id= ?;";
        String prepDelete2 = "DELETE FROM users WHERE id= ?;";
        String prepQuery3 = "SELECT id FROM timeslots WHERE id_doctor = ?;";
        PreparedStatement pr1 = conn.prepareStatement(prepDelete1);
        pr1.setObject(1, id);

        PreparedStatement pr2 = conn.prepareStatement(prepDelete2);
        pr2.setObject(1, id);

        PreparedStatement pr3 = conn.prepareStatement(prepQuery3);
        pr3.setObject(1, id);
        ResultSet r = pr3.executeQuery();
        if (r.getFetchSize() != 0) {
            logger.info("Doctor "+id+" has appointments");
            throw new DependencyExeption("Doctor "+id+" has appointments");
        }
        int rs = pr1.executeUpdate();
        pr2.executeUpdate();

        if (rs == 0) {
            logger.info("Doctor "+id+" not found");
            throw new IllegalArgumentException("Doctor "+id+" not found");
        }
    }

    @Override
    public void save(Doctor doctor) throws SQLException, ClassNotFoundException {
        if (doctor.getId() == null) {
            logger.info("trying save doctor" + doctor);
            Connection conn = DataBaseConnection.getConnection();
            User user=new User();
            user.setId(doctor.getId());
            user.setEmail(doctor.getEmail());
            user.setPassword(doctor.getPassword());
            Role role = new Role();
            role.setRoleId(1);
            role.setName("DOCTOR");
            user.setRole(role);
            Long id = new  UserDAO().addUser(user);
            String preparedQuerry1 = "INSERT INTO doctors (id,first_name,last_name,start_time,end_time) VALUES(?,?,?,?,?)";
            PreparedStatement preparedStmt = conn.prepareStatement(preparedQuerry1);
            preparedStmt.setObject(1,id);
            preparedStmt.setString(2, doctor.getFirstName());
            preparedStmt.setString(3, doctor.getLastName());
            preparedStmt.setTime(4,  Time.valueOf(doctor.getStartTime()));
            preparedStmt.setTime(5,  Time.valueOf(doctor.getEndTime()));
            preparedStmt.executeUpdate();

            logger.info("Added "+doctor.toString());
        }
    }

    @Override
    public void update(Doctor doctor) throws SQLException, ClassNotFoundException {
        Connection conn = DataBaseConnection.getConnection();
        String prepUpdate1 = "UPDATE doctors SET first_name=?,last_name =?, start_time =?, end_time=?" +
                " WHERE id=?;";

        PreparedStatement pr1 = conn.prepareStatement(prepUpdate1);
        pr1.setString(1, doctor.getFirstName());
        pr1.setString(2, doctor.getLastName());
        pr1.setTime(3, Time.valueOf(doctor.getStartTime()));
        pr1.setTime(4, Time.valueOf(doctor.getEndTime()));
        pr1.setObject(5, doctor.getId());
        int res = pr1.executeUpdate();

        String prepUpdate2 = "UPDATE users SET email=?,password =?" +
                " WHERE id=?;";

        PreparedStatement pr2 = conn.prepareStatement(prepUpdate2);
        pr2.setString(1, doctor.getEmail());
        pr2.setString(2, doctor.getPassword());
        pr2.setObject(3, doctor.getId());
        int res2 = pr2.executeUpdate();

        if (res == 0){
            logger.error(doctor.toString()+" not found");
            throw new IllegalArgumentException(doctor.toString()+" not found");
        }
        logger.info("Updated : "+doctor.toString());
    }


}
