package DAO;

import DAO.interfaces.IDaoUser;
import DB.DataBaseConnection;
import exeptions.DependencyExeption;
import models.User;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDAO implements IDaoUser {

    private static final String USER_ID_COLUMN_NAME = "id";
    private static final String USER_EMAIL_COLUMN_NAME = "email";
    private static final String USER_PASSWORD_COLUMN_NAME = "password";
    private static final String USER_ROLE_ID_COLUMN_NAME = "role";


    private static final String GET_USER_BY_ID_REQUEST = "SELECT * FROM users WHERE " + USER_ID_COLUMN_NAME + "=?";
    private static final String ADD_USER_REQUEST = "INSERT INTO users(" +
            USER_EMAIL_COLUMN_NAME + ", " + USER_PASSWORD_COLUMN_NAME + ", " +
            USER_ROLE_ID_COLUMN_NAME +
            ") VALUES(?, ?, ?)";
    private static final String DROP_USER_BY_ID_REQUEST = "DELETE FROM user WHERE " + USER_ID_COLUMN_NAME + "=?";
    private static final String UPDATE_USER_BY_ID_REQUEST = "UPDATE users SET " +
            USER_EMAIL_COLUMN_NAME + "=?, " + USER_PASSWORD_COLUMN_NAME + "=?, " +
            USER_ROLE_ID_COLUMN_NAME + "=? " +
            "WHERE user_id=?";
    private static final String GET_ALL_USER_REQUEST = "SELECT * FROM users";
    private static final String GET_USER_BY_EMAIL = "SELECT * FROM users WHERE " + USER_EMAIL_COLUMN_NAME + "=?";
    private static Logger logger = Logger.getLogger(UserDAO.class);

    @Override
    public User getUserById(Long id)  throws SQLException, ClassNotFoundException {
        logger.info("Get User id " + id);
        Connection conn = DataBaseConnection.getConnection();
        PreparedStatement preparedStatement = conn.prepareStatement(GET_USER_BY_ID_REQUEST);
        preparedStatement.setLong(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        User user = null;
        if (resultSet.next()) {
            user = new User();
            user.setId(resultSet.getLong(USER_ID_COLUMN_NAME));
            user.setEmail(resultSet.getString(USER_EMAIL_COLUMN_NAME));
            user.setPassword(resultSet.getString(USER_PASSWORD_COLUMN_NAME));
            user.setRole(new RoleDAO().getRoleById(resultSet.getInt(USER_ROLE_ID_COLUMN_NAME)));
        }
        if (user.getRole().isRoleDoctor()) {
            String sql = "SELECT * FROM doctors WHERE id = ?";
            PreparedStatement preparedStatement1 = conn.prepareStatement(sql);
            preparedStatement1.setLong(1, id);
            ResultSet resultSet1 = preparedStatement1.executeQuery();
            if (resultSet1.next()) {
                user.setFirstName(resultSet1.getString("first_name"));
                user.setLastName(resultSet1.getString("last_name"));
            }
        }
        if (user.getRole().isRolePatient()) {
            String sql = "SELECT * FROM patients WHERE id = ?";
            PreparedStatement pr2 = conn.prepareStatement(sql);
            pr2.setLong(1, id);
            ResultSet rs2 = pr2.executeQuery();
            if (rs2.next()) {
                user.setFirstName(rs2.getString("first_name"));
                user.setLastName(rs2.getString("last_name"));
            }
        }
        return user;
    }



    @Override
    public Long addUser(User user) throws SQLException, ClassNotFoundException {
        logger.info("Try save user  " + user);
        if (user.getId() == null) {
            Connection conn = DataBaseConnection.getConnection();
            String preparedQuerry1 = "INSERT INTO users (email,password,role) VALUES(?,?,?)";
            PreparedStatement preparedStmt = conn.prepareStatement(preparedQuerry1);
            preparedStmt.setString(1, user.getEmail());
            preparedStmt.setString(2, user.getPassword());
            preparedStmt.setInt(3, user.getRole().getRoleId());
            preparedStmt.executeUpdate();
            String preparedQuery1 = "SELECT id FROM users WHERE email=?;";
            PreparedStatement pr2 = conn.prepareStatement(preparedQuery1);
            pr2.setString(1, user.getEmail());
            ResultSet rs = pr2.executeQuery();
            Long id = null;
            if (rs.next()) {
                id = rs.getLong("id");
            }
            return id;
        } else return user.getId();

    }


    @Override
    public void deleteUserById(long id) throws SQLException, ClassNotFoundException, DependencyExeption {
        logger.info("Try delete user  " +  id);
        Connection conn = DataBaseConnection.getConnection();
        String prepQuery = "SELECT * FROM doctors WHERE id= ?;";
        PreparedStatement prS = conn.prepareStatement(prepQuery);
        prS.setLong(1, id);
        ResultSet r = prS.executeQuery();
        if (r.getFetchSize() != 0) {
            logger.warn("User "+id+" have dependency in doctor table");
            throw new DependencyExeption("User "+id+" have dependency in doctor table");
        }

        String prepQuery2= "SELECT * FROM patients WHERE id= ?;";
        PreparedStatement prS2 = conn.prepareStatement(prepQuery2);
        prS2.setLong(1, id);
        r = prS2.executeQuery();
        if (r.getFetchSize() != 0) {
            logger.warn("User "+id+"have dependency in patient table");
            throw new DependencyExeption("User "+id+"have dependency in patient table");
        }

        String prepDelete = "DELETE FROM users WHERE id= ?;";
        PreparedStatement prd = conn.prepareStatement(prepDelete);
        prd.setLong(1, id);
        int rs=prd.executeUpdate();
        if (rs == 0) {
            logger.warn("Doctor "+id+" not found");
            throw new IllegalArgumentException("Doctor "+id+" not found");
        }
    }

    @Override
    public void updateUserById(User user) throws SQLException, ClassNotFoundException {
        logger.info("try update user " + user);
        Connection connection = DataBaseConnection.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_BY_ID_REQUEST);
        preparedStatement.setString(1, user.getEmail());
        preparedStatement.setString(2, user.getPassword());
        preparedStatement.setInt(3, user.getRole().getRoleId());
        preparedStatement.setLong(4, user.getId());
        preparedStatement.executeUpdate();



    }


    @Override
    public List<User> getAllUser() throws SQLException, ClassNotFoundException {
        logger.info("try get all users ");
        Connection connection = DataBaseConnection.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_USER_REQUEST);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<User> users = new ArrayList<>();

        while (resultSet.next()) {
            users.add(getUserById(resultSet.getLong(USER_ID_COLUMN_NAME)));
        }
        if (users.isEmpty())
            return null;
        logger.info("try get all users " + users);
        return users;
    }
    @Override
    public User getUserByEmail(String email) throws SQLException, ClassNotFoundException {
        Connection conn = DataBaseConnection.getConnection();
        logger.info("try user by email " + email);
        PreparedStatement preparedStatement = conn.prepareStatement(GET_USER_BY_EMAIL);
        preparedStatement.setString(1, email);
        ResultSet resultSet = preparedStatement.executeQuery();
        User user = null;
        if (resultSet.next()) {
            user = new User();
            user.setId(resultSet.getLong(USER_ID_COLUMN_NAME));
            user.setEmail(resultSet.getString(USER_EMAIL_COLUMN_NAME));
            user.setPassword(resultSet.getString(USER_PASSWORD_COLUMN_NAME));
            user.setRole(new RoleDAO().getRoleById(resultSet.getInt(USER_ROLE_ID_COLUMN_NAME)));
            if (user.getRole().isRoleDoctor()) {
                String sql = "SELECT * FROM doctors WHERE id = ?";
                PreparedStatement preparedStatement1 = conn.prepareStatement(sql);
                preparedStatement1.setLong(1, user.getId());
                ResultSet resultSet1 = preparedStatement1.executeQuery();
                if (resultSet1.next()) {
                    user.setFirstName(resultSet1.getString("first_name"));
                    user.setLastName(resultSet1.getString("last_name"));
                }
            }
            if (user.getRole().isRolePatient()) {
                String sql = "SELECT * FROM patients WHERE id = ?";
                PreparedStatement pr2 = conn.prepareStatement(sql);
                pr2.setLong(1, user.getId());
                ResultSet rs2 = pr2.executeQuery();
                if (rs2.next()) {
                    user.setFirstName(rs2.getString("first_name"));
                    user.setLastName(rs2.getString("last_name"));
                }
            }

        }
        logger.info("try get  user " + user);
        return user;
    }


    private User getUser(ResultSet resultSet) throws SQLException, ClassNotFoundException {
        User user = null;
        if (resultSet.next()) {
            user = new User();
            user.setId(resultSet.getLong(USER_ID_COLUMN_NAME));
            user.setEmail(resultSet.getString(USER_EMAIL_COLUMN_NAME));
            user.setPassword(resultSet.getString(USER_PASSWORD_COLUMN_NAME));
            user.setRole(new RoleDAO().getRoleById(resultSet.getInt(USER_ROLE_ID_COLUMN_NAME)));
        }
        return user;
    }
    @Override
    public boolean isUserInDB(String email) throws SQLException, ClassNotFoundException {
        Connection conn = DataBaseConnection.getConnection();
        logger.info("is user in db " + email);
        String prepQuerry1 = "SELECT * FROM users WHERE email = ?";
        PreparedStatement pr1 = conn.prepareStatement(prepQuerry1);
        pr1.setObject(1, email);
        ResultSet rs1 = pr1.executeQuery();
        if (rs1.next()) {
            return true;
        }
        else {
            return false;
        }
    }

}
