package validator;

import models.TimeSlot;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalTime;

public class TimeSlotValidator implements ConstraintValidator<validator.TimeSlot, TimeSlot> {
    public void initialize(TimeSlot constraintAnnotation) {
    }

    @Override
    public boolean isValid(TimeSlot timeSlot, ConstraintValidatorContext constraintValidatorContext)  {
        LocalTime startTime = timeSlot.getFrom();
        LocalTime endTime = timeSlot.getTo();
        if (startTime.isBefore(endTime)&&startTime.isAfter(LocalTime.now())) {
            return true;
        }
        return false;
    }
}
