package validator;

import org.joda.time.LocalDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateBirthValidator implements ConstraintValidator<DateBirth, LocalDate> {

    private final int yearsBeforeNow=120;
    private final int daysBeforeNow=1;
    private LocalDate minDate;
    private LocalDate maxDate;

    @Override
    public void initialize(DateBirth constraintAnnotation) {
        LocalDate now=LocalDate.now();
        this.minDate = now.minusYears(yearsBeforeNow);
        this.maxDate=now.minusDays(daysBeforeNow);
    }
    @Override
    public boolean isValid(LocalDate date, ConstraintValidatorContext constraintValidatorContext) {
        return (date.isAfter(minDate)&& date.isBefore(maxDate));
    }
}