<%@ page import="models.Doctor" %>
<%@ page import="models.User" %>
<%@ page import="DAO.DoctorDAO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PersonalPage</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/flaticon.css"/>
    <link rel="stylesheet" href="css/owl.carousel.css"/>
    <link rel="stylesheet" href="css/magnific-popup.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
</head>
<body>
<div class="site-wrap">

    <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>


    <div class="site-navbar-wrap">

        <div class="site-navbar">
            <div class="container py-1">
                <div class="row align-items-center">
                    <div class="col-2">
                        <h2 class="mb-0 site-logo"><a href="login.jsp">DoctorServise</a></h2>
                    </div>
                    <div class="col-10">
                        <nav class="site-navigation text-right" role="navigation">
                            <div class="container">
                                <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span
                                        class="icon-menu h3"></span></a></div>
                                <ul class="site-menu js-clone-nav d-none d-lg-block">
                                    <li class="has-children">
                                        <a href="patients.html">Log in</a>
                                        <ul class="dropdown arrow-top">
                                            <% if (session.getAttribute("User") == null) {%>
                                            <li><a href="<c:url value = "/login.jsp"/>">Log in</a></li>
                                            <li><a href="<c:url value = "/reg.jsp"/>">Registration</a></li>
                                            <%}%>
                                            <% User user = (User) session.getAttribute("User");
                                                Doctor doctor = new DoctorDAO().findById(user.getId());
                                                if (user != null) {%>
                                            <li class=""><a href="<c:url value="/logout"/>" class="nav-link">Log out</a>
                                            </li>
                                            <%}%>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-blocks-cover inner-page" style="background-image: url(images/hero_bg_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <h1> <strong>Your personal page</strong></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="home" class="header">
    <div class="container">
        <div class="top-header">

            <div class="top-nav">
                <div class="navigation">
                    <div class="clearfix"></div>
                </div>
                <!-- /top-hedader -->
            </div>
            <div class="banner-info">
                <div class="row">
                <div class="col-md-7 header-right">
                    <h2>DOCTOR</h2>
                    <ul class="address">

                        <li>
                            <ul class="address-text">
                                <li><b>NAME</b></li>
                                <li><strong><%=user.getFirstName()%> <%=user.getLastName()%></strong></li>
                            </ul>
                        </li>
                        <li>
                            <ul class="address-text">
                                <li><b>START TIME</b></li>
                                <li><%=doctor.getStartTime()%></li>
                            </ul>
                        </li>
                        <li>
                            <ul class="address-text">
                                <li><b>END TIME </b></li>
                                <li><%=doctor.getEndTime()%></li>
                            </ul>
                        </li>

                        <li>
                            <ul class="address-text">
                                <li><b>E-MAIL </b></li>
                                <li><a href="mailto:example@mail.com"> <%=user.getEmail()%></a></li>
                            </ul>
                        </li>

                    </ul>
                </div>

                <div class="col-md-5 header-left">
                    <h1><strong>YOUR APPOINTMENTS</strong></h1>
                    <c:forEach items="<%= doctor.getTimeList()%>" var="timeSlot">
                        <ol class="rectangle-list">
                            <li>
                                <div class="wrapper-li">
                                    <a href="" class="mClass2 " data-toggle="modal" data-id="${timeSlot.id}" data-target="#exampleModa4">
                                        <c:out value="${timeSlot.from}"/> - <c:out value="${timeSlot.to}"/> - <c:out value="${timeSlot.patient.lastName}"/> </a>
                                    <a href="" class="update-patient" data-toggle="modal" data-target="#exampleModal" style="font-size: 30px; color: black;" ></a>
                                </div>
                            </li>
                        </ol>
                    </c:forEach>

                </div>
                <div class="clearfix"></div>

            </div>
                <div class="col-md-5 header-left">

                    <button type="button" class="btn btn--green" data-toggle="modal" data-target="#exampleModa2">
                        Update
                    </button>
                    <button type="button" class="btn btn--green" data-toggle="modal" data-target="#exampleModa3">
                        Delete
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="site-footer border-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 mb-5 mb-lg-0">
            </div>
            <div class="col-lg-4 mb-5 mb-lg-0">
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <a href="#" class="pl-0 pr-lg-5"><span class="icon-facebook"></span></a>
                            <a href="#" class="pl-3 pr-lg-5"><span class="icon-twitter"></span></a>
                            <a href="#" class="pl-3 pr-lg-5"><span class="icon-instagram"></span></a>
                            <a href="#" class="pl-3 pr-lg-5"><span class="icon-linkedin"></span></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>
<div id="dropDownSelect1"></div>

<div class="modal fade" id="exampleModa2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabe2"><strong>UPDATE</strong></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="${pageContext.request.contextPath}/updateDoctor" method="post">
                <div class="modal-body">
                    <div class="input-group">
                        <input class="input--style-3" type="text" placeholder="First Name" name="firstName">
                    </div>
                    <div class="input-group">
                        <input class="input--style-3" type="text" placeholder="Last Name" name="lastName">
                    </div>
                    <div class="input-group">
                        <input class="input--style-3" type="email" placeholder="Email" name="email">
                    </div>
                    <div class="input-group">
                        <input class="input--style-3" type="time" name="start" id="start">
                    </div>
                    <div class="input-group">
                        <input class="input--style-3" type="time" name="end" id="end">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="exampleModa3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabe3"><strong>ARE U WANT DELETE YOUR ACCOUNT?</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="${pageContext.request.contextPath}/DeleteDoctorController">
                <div class="modal-body">
                    <div class="input-group">
                        <input class="input--style-3" type="password" placeholder="Password" name="password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-danger" value="ABORT">
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="exampleModa4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabe4"><strong>ARE U WANT DELETE YOUR TIMESLOT</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="${pageContext.request.contextPath}/deleteTimeSlotDoctor">
                <div class="modal-body">
                    <div class="input-group">
                        <input class="input--style-3" type="password" placeholder="Password" name="password">
                        <input type="hidden" id="idDoctor" name="idDoctor">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-danger" value="ABORT">
                </div>
            </form>
        </div>
    </div>
</div>



<script src="js/main.js"></script>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/aos.js"></script>
<script src="js/main.js"></script>

<script>
    $(document).on("click", ".mClass2", function () {
        var id = $(this).data('id');
        $("#idDoctor").val(id);
    })
</script>
</body>
</html>