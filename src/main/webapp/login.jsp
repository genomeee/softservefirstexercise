<%@ page import="models.User" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body style="background: url(images/hero_bg_1.jpg) no-repeat fixed; width: 100%;height: 100%;background-size: 100%;">
<div class="site-wrap">

    <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->


    <div class="site-navbar-wrap">
        <div class="site-navbar-top">
            <div class="container py-2">
                <div class="row align-items-center">
                    <div class="col-6">

                    </div>
                </div>
            </div>
        </div>
        <div class="site-navbar">
            <div class="container py-1">
                <div class="row align-items-center">
                    <div class="col-2">
                        <h2 class="mb-0 site-logo"><a href="login.jsp">DoctorServise</a></h2>
                    </div>
                    <div class="col-10">
                        <nav class="site-navigation text-right" role="navigation">
                            <div class="container">
                                <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>
                                <ul class="site-menu js-clone-nav d-none d-lg-block">
                                    <li><a href="aboutAs.jsp">About Us</a></li>
                                    <li><a href="<c:url value="/allDoctors"/>">Doctors</a></li>
                                    <li class="has-children">
                                        <a href="patients.html">Log in</a>
                                        <ul class="dropdown arrow-top">
                                            <% if (session.getAttribute("User") == null ) {%>
                                            <li><a href ="<c:url value = "/login.jsp"/>" >Log in</a></li>
                                            <li><a href="<c:url value = "/reg.jsp"/>" >Registration</a></li>
                                            <%}%>
                                            <% User user = (User) session.getAttribute("User");
                                                if (user != null && user.getRole()!=null){%>
                                            <li class=""><a href="<c:url value="/logout"/>" class="nav-link">Log out</a>
                                                <b>${user.getRole().toString()} ${user.getFirstName()} ${user.getLastName()}</b></li>
                                            <%}%>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="wrapper wrapper--w780">
    <div class="card card-3">

        <div class="card-body">
            <h2 class="title">Log in</h2>
            <% if (request.getAttribute("loginError") != null) {%>
            <div class="error-doctor">
                <%=request.getAttribute("loginError") %>
            </div>
            <br>
            <% } %>
            <form method="POST" action="login">
                <div class="input-group">
                    <input class="input--style-3" type="email" placeholder="Email" name="email">
                </div>
                <div class="input-group">
                    <input class="input--style-3" type="password" placeholder="Password" name="pass">
                </div>
                <div class="p-t-10">
                    <button class="btn btn--pill btn--green" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="js/main.js"></script>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>
<script src="js/aos.js"></script>

<script src="js/main.js"></script>
</body>
</html>