CREATE TABLE doctors
(
  id integer NOT NULL,
  first_name character varying(20) NOT NULL,
  last_name character varying(20) NOT NULL,
  start_time time without time zone NOT NULL,
  end_time time without time zone NOT NULL,
  CONSTRAINT doctor_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE doctors
  OWNER TO postgres;

CREATE TABLE patients
(
  id integer NOT NULL,
  first_name character varying(20) NOT NULL,
  last_name character varying(20) NOT NULL,
  date_of_birth date NOT NULL,
  phone character varying(20) NOT NULL,
  CONSTRAINT patient_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patients
  OWNER TO postgres;

CREATE TABLE roles
(
  id serial NOT NULL,
  name character varying(255),
  CONSTRAINT roles_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE roles
  OWNER TO postgres;
CREATE TABLE timeslots
(
  id serial NOT NULL,
  id_doctor integer NOT NULL,
  id_patient integer NOT NULL,
  start_time time without time zone NOT NULL,
  end_time time without time zone NOT NULL,
  CONSTRAINT timeslots_pkey PRIMARY KEY (id),
  CONSTRAINT timeslots_id_doctor_fkey FOREIGN KEY (id_doctor)
      REFERENCES doctors (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT timeslots_id_patient_fkey FOREIGN KEY (id_patient)
      REFERENCES patients (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE timeslots
  OWNER TO postgres;

CREATE TABLE users
(
  id serial NOT NULL,
  email character varying(255),
  password character(255),
  role integer,
  CONSTRAINT users_pkey PRIMARY KEY (id),
  CONSTRAINT users_roles_fkey FOREIGN KEY (role)
      REFERENCES roles (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO postgres;